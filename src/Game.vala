using SFML.Window;
using SFML.Graphics;


public class Game {

    public Level level { get; private set; } 
    public LevelRenderer level_renderer { get; private set; }

    private UserInterface ui;

    public Game () {
        ui = new UserInterface (this);
        level = new Level.empty (new IslandGenerator ());
        level_renderer = new LevelRenderer (level);
    }

    public void render (RenderBundle bundle) {
        level_renderer.render (bundle);
        ui.render (bundle);
        MousePointer.get_instance ().render (bundle);
    }

    public void update (float dt) {
        ui.update (dt);
        level.update (dt);
        MousePointer.get_instance ().update (dt);
    }

    public void pass_event (Event e) {
        ui.pass_event (e);
    }

}
