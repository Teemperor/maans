using SFML.Graphics;
using SFML.Window;

public struct MoveCameraResult {
    bool x_limited;
    bool y_limited;
}

public class LevelRenderer {

    Level level;

    RectangleShape water_rect = new RectangleShape ();

    RenderTexture render_texture;
    RenderTexture final_texture;
    RenderTexture water_texture;
    RenderTexture light_texture;

    Texture* last_tex = null;

    static FloatRect rup = {0, 0, MainWindow.get_instance ().width, MainWindow.get_instance ().height};
    public View view { get; private owned set; }

    public LevelRenderer (Level level) {
        this.level = level;

        view = new View.from_rect (rup);

        water_rect.size = {MainWindow.get_instance ().width, MainWindow.get_instance ().height};

        render_texture = new RenderTexture (MainWindow.get_instance ().width, MainWindow.get_instance ().height, false);
        final_texture = new RenderTexture (MainWindow.get_instance ().width, MainWindow.get_instance ().height, false);
        water_texture = new RenderTexture (MainWindow.get_instance ().width, MainWindow.get_instance ().height, false);
        light_texture = new RenderTexture (MainWindow.get_instance ().width, MainWindow.get_instance ().height, false);

    }



    public MoveCameraResult move_camera (int dx, int dy) {
        float tx = view.center.x + dx;
        float ty = view.center.y + dy;
        MoveCameraResult result = {false, false};

        if (tx < view.size.x/2) {
            tx = view.size.x/2;
            result.x_limited = true;
        }

        if (ty < view.size.y/2) {
            ty = view.size.y/2;
            result.y_limited = true;
        }

        if (tx >= level.width * Tile.SIZE - view.size.x/2) {
            tx = (level.width) * Tile.SIZE - view.size.x/2 - 1;
            result.x_limited = true;
        }

        if (ty >= level.height * Tile.SIZE - view.size.y/2) {
            ty = (level.height) * Tile.SIZE - view.size.y/2 - 1;
            result.y_limited = true;
        }

        view.center = {(int) tx, (int) ty};

        return result;
    }

    public void render (RenderBundle bundle) {
        render_texture.set_view (view);
        light_texture.set_view (view);

        // Move the view to integer-coordinates to prevnet graphical glitches
        view.center = { (int) view.center.x, (int) view.center.y };

        render_water (bundle);

        switch_to (render_texture);

        last_tex = water_texture.get_texture ();
        Sprite water_sprite = new Sprite ();
        water_sprite.set_texture_full (last_tex, false);
        water_sprite.position = {view.center.x - view.size.x/2, view.center.y - view.size.y/2};
        render_texture.draw_sprite (water_sprite, null);

        Point upper_left = level.to_valid_tile_coords (view.center.x - view.size.x/2, view.center.y - view.size.y/2);
        Point down_right =  level.to_valid_tile_coords (view.center.x + view.size.x/2, view.center.y + view.size.y/2);

        Tile* tile = null;
        for (int x = upper_left.x; x < down_right.x + 1; x++) {
            for (int y = upper_left.y; y < down_right.y + 1; y++) {
                tile = level.get_tile_p (x,y);
                if (tile->data.dummy)
                    continue;
                unowned Sprite sprite = tile->get_sprite ();
                sprite.position = {x*Tile.SIZE, y*Tile.SIZE};
                render_texture.draw_sprite (sprite, null);
            }
        }

        FloatRect view_rect = {view.center.x - view.size.x/2, view.center.y - view.size.y/2,
                               view.size.x, view.size.y};

        level.grid.foreach_in_float_rect ((e) => {
            //if (e.in_rect (view_rect))
               e.render (new RenderBundle(render_texture, null, bundle.dt));
        }, view_rect);


        switch_away (render_texture);
        switch_to (light_texture);

        light_texture.clear (Color.transparent);
        level.grid.foreach_in_float_rect ((e) => {
            //if (e.in_rect (view_rect));
                e.render_light (new RenderBundle(light_texture, bundle.window, bundle.dt));
        }, view_rect);
        switch_away (light_texture, bundle.window);


        ShaderCollection.light_shader.set_texture_param ("texture", render_texture.get_texture ());
        ShaderCollection.light_shader.set_texture_param ("light_map", light_texture.get_texture ());
        ShaderCollection.light_shader.set_float_param ("daylight", 1 - 0.9f * (float) Math.cos (Maans.float_time/3));
        Shader.bind (ShaderCollection.light_shader);
        final_texture.draw_rectangle_shape (water_rect, null);
        Shader.bind (null);

        switch_away (render_texture, bundle.window);

        Sprite tsprite = new Sprite ();
        last_tex = final_texture.get_texture ();
        tsprite.set_texture_full (last_tex, false);
        /*
        tsprite.scale = {1.0f, -1.0f};
        tsprite.position = {0, view.size.y };*/

        tsprite.texture_rect = {0,
                                (int) tsprite.local_bounds.height,
                                (int) tsprite.local_bounds.width,
                                -(int)tsprite.local_bounds.height};

        bundle.window->draw_sprite (tsprite, null);

        level.try_lock_renderer ();

        Shader.bind (null);
    }

    private void render_water (RenderBundle bundle) {
        switch_to (water_texture, bundle.window);

        ShaderCollection.water_shader.set_float_param ("itime", Maans.float_time);
        ShaderCollection.water_shader.set_float_param ("offx", view.center.x);
        ShaderCollection.water_shader.set_float_param ("offy", -view.center.y);
        Shader.bind (ShaderCollection.water_shader);
        water_texture.draw_rectangle_shape (water_rect, null);
        Shader.bind (null);

        switch_away (water_texture);
    }

    private void switch_to (RenderTexture tex, Window? win = null) {
        if (win != null)
            win.active =  false;
        tex.active = true;
    }

    private void switch_away (RenderTexture tex, Window? win = null) {
        tex.display ();
        tex.active = false;
        if (win != null)
            win.active = true;
    }

}
