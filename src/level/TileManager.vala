using SFML.Graphics;

public class TileData : AssetData {

    public TileData (Sprite* sprite, Regex? regex, string base_type, bool dummy, bool passable) {
        this.sprite = sprite;
        this.regex = regex;
        this.base_type = base_type;
        this.dummy = dummy;
        this.passable = passable;
    }

    public bool dummy { get; private set; }
    public Sprite* sprite { get; private set; }
    public Regex regex { get; private set; }
    public string base_type { get; private set; }
    public bool passable { get; private set; }

    public static AssetManager manager { get; private set; }

    public static void init () {
        manager = new AssetManager ("tile", ((conf) => {
            try {
                bool dummy = false;

                string name = conf.name;

                if (conf.has_key ("Tile", "dummy"))
                    dummy = conf.get_bool ("Tile", "dummy");

                bool passable = true;

                try {
                    passable = conf.get_bool ("Tile", "passable");
                } catch (Error e) {
                }

                if (dummy) {
                    return new TileData (null, null, name, true, false);
                } else {
                    Sprite* sprite = Loader.get_instance ().sprite ("tile", conf.get_path ("Tile", "texture"));
                    string base_type = "NONE";
                    try {
                        base_type = conf.get_string ("Tile", "type");
                    } catch (Error e) {
                        //TODO
                    }

                    Regex regex = new Regex ("");
                    try {
                        string raw_regex = "^" + base_type + "," + conf.get_string ("Tile", "regex");
                        raw_regex = raw_regex.replace ("$ALL", "([a-zA-Z0-9_])+");
                        regex = new Regex (raw_regex, RegexCompileFlags.OPTIMIZE);
                    } catch (Error e) {
                        error (e.message);
                        //TODO
                    }

                    return new TileData (sprite, regex, base_type, false, passable);
                }
            } catch (Error e) {
                error (e.message);
            }
        }));

    }

    public static TileData? from_env (string env) {
        TileData[] results = {};
        manager.foreach_asset ((raw_data) => {
            TileData data = (TileData) raw_data;
            if (data.dummy)
                return true;

            if (data.regex.match (env)) {
                results += data;
            }
            return true;
        });
        if (results.length == 0)
            return null;
        return results[Random.int_range (0, results.length)];
    }
}
