using SFML.Graphics;
using SFML.Window;
using SFML.System;

public class Level {


    public int width { get; private set; }
    public int height { get; private set; }

    public ChunkGrid grid { get; private set; }

    Mutex renderer_to_fast;
    bool logic_ready = false;

    bool start_logic = false;

    public Weather weather { get; private set; }

    Tile[,] tiles;

    LevelProvider provider;

    public uint time { get; private set; }


    public Level.empty (LevelProvider provider) {
        this.provider = provider;
        this.width = provider.get_level_dimensions ().x;
        this.height = provider.get_level_dimensions ().y;
        weather = provider.get_weather ();

        renderer_to_fast = Mutex ();

        tiles = new Tile[width, height];

        update_async ();
        grid = new ChunkGrid (width, height);
    }

    public void post_init () {
        provider.create (this);
    }

    private async void update_func () throws ThreadError {
        ThreadFunc<void*> run = () => {
            while (true) {
                while (true) {
                    lock (start_logic) {
                        if (start_logic) {
                            renderer_to_fast.lock ();
                            start_logic = false;
                            break;
                        }
                    }
                    //ugly busywaiting till i have a clue why the Cond is not working
                    Thread.usleep (1);
                }

                //update_cond.wait (update_mutex);

                update_work ();
                renderer_to_fast.unlock ();
            }
        };
        new Thread<void*>("test", run);
        yield;
    }

    public void update_async () {
        update_func.begin ();
    }

    public bool is_passable (int x, int y) {
        if (!valid_coord (x,y))
            return false;
        return tiles[x,y].data.passable;
    }

    public void try_lock_renderer () {
        if (renderer_to_fast.trylock ())
            renderer_to_fast.unlock ();
    }

    public void eval (int x, int y) {
        string env = "";
        env += get_base_type (x, y - 1) + ",";
        env += get_base_type (x + 1, y - 1) + ",";
        env += get_base_type (x + 1, y) + ",";
        env += get_base_type (x + 1, y + 1) + ",";
        env += get_base_type (x, y + 1) + ",";
        env += get_base_type (x - 1, y + 1) + ",";
        env += get_base_type (x - 1, y) + ",";
        env += get_base_type (x - 1, y - 1);
        tiles[x, y].eval (env);
    }

    public string get_base_type (int x, int y) {
        if (!valid_coord (x,y))
            return "none";

        string result = tiles[x,y].data.base_type;
        return result;
    }

    public void set_tile (int x, int y, Tile t) {
        tiles[x,y] = t;
    }

    public Tile get_tile (int x, int y) {
        return tiles[x,y];
    }
    
    public Tile* get_tile_p (int x, int y) {
        return &tiles[x, y];
    }

    public bool valid_coord (int x, int y) {
        return !(x < 0 || y < 0 || x >= width || y >= height);
    }

    public bool is_passable_point (Point p) {
        return is_passable (p.x, p.y);
    }

    float lambda_dt = 0;
    public void update (float dt) {
        lambda_dt = dt;
        logic_ready = false;
        //TODO more precise time updating instead of transforming seconds->millis
        time += (uint) (dt * 1000);
        lock (start_logic) {
            start_logic = true;
        }
        //update_cond.signal ();
        //update_work ();
    }

    public void update_work () {
        grid.foreach_chunk ((e) => {
            e.update (lambda_dt);
        });
        logic_ready = true;
    }

    public Point to_valid_tile_coords (float x, float y) {
        int tx = Tile.to_tile_coords (x);
        int ty = Tile.to_tile_coords (y);
        if (tx < 0)
            tx = 0;
        else if (tx >= width)
            tx = width -1;

        if (ty < 0)
            ty = 0;
        else if (ty >= height)
            ty = height;

        return new Point (tx, ty);
    }
}
