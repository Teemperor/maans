

public class IslandGenerator : LevelProvider {

    Level level;

    public override SFML.System.Vector2i get_level_dimensions () {
        return {16 * 20, 16 * 20};
    }


    public override Weather get_weather () {
        return new Weather ();
    }

    public override void create (Level level) {
        this.level = level;
        message ("Starting Level post-init");
        message ("Creating landscape");
        for (int x = 0; x < level.width; x++) {
            for (int y = 0; y < level.height; y++) {
                level.set_tile (x, y, Tile ("w"));
            }
        }


        for (int j = 0; j < 120; j++) {
            int px = level.width/2;
            int py = level.height/2;

            for (int i = 0; i < 1000; i++) {
                px += Random.int_range (-3, 4);
                py += Random.int_range (-3, 4);

                if (px < 8 || py < 8 || px > level.width -8 || py > level.height - 8)
                    break;

                paint_level ("g", px, py, 3);
            }
        }


        for (int x = 0; x < level.width; x++) {
            for (int y = 0; y < level.height; y++) {
                level.eval (x, y);
            }
        }

        message ("Creating Enemies");
        for(int i = 0; i < 1; i++) {
            new Human (level, "worker", level.width/2 * Tile.SIZE, level.height/2 * Tile.SIZE);
        }

        message ("Create Trees");
        for (int x = 0; x < level.width - 1; x++) {
            for (int y = 0; y < level.height - 1; y++) {
                if (level.get_tile (x, y).data.name == "grass1"
                    && Random.int_range (0, 100) < 1) {
                    new Tree ("default", level, x * Tile.SIZE, y * Tile.SIZE);
                }
            }
        }
        message ("Level post-init OK!");
    }

    public void paint_level (string type, int x, int y, int size) {
        for(int tx = x - size; tx < x + size; tx++) {
            for(int ty = y - size; ty < y + size; ty++) {
                level.set_tile (tx, ty, Tile (type));
            }
        }
    }

}
