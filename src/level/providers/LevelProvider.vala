

public abstract class LevelProvider {

    public abstract SFML.System.Vector2i get_level_dimensions ();

    public abstract Weather get_weather ();

    public abstract void create (Level level);

}
