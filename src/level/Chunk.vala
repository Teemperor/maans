using SFML.Graphics;

public delegate bool SearchFunc<T> (T x);

public class ChunkGrid {

    private Chunk[,] chunks;

    int width;
    int height;

    public ChunkGrid (int xtiles, int ytiles) {
        width = tile_to_chunk (xtiles);
        height = tile_to_chunk (ytiles);

        chunks = new Chunk[width, height];

        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                chunks[x,y] = new Chunk ();
            }
        }
    }

    public void foreach_chunk (Func<Entity> func) {
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                chunks[x,y].@foreach (func);
            }
        }
    }

    public void search_entities_from_entity (Entity e, SearchFunc<Entity> search_func, int max_dist = -1) {
        search_entities_from_pos (e.x, e.y, search_func, max_dist);
    }

    public void search_entities_from_pos (float x, float y, SearchFunc<Entity> search_func, int max_dist = -1) {
        int tx = float_to_chunk (x);
        int ty = float_to_chunk (y);

        bool left_limit = false;
        bool right_limit = false;
        bool upper_limit = false;
        bool lower_limit = false;

        int dist = 1;

        if (chunks[tx, ty].search_entities (search_func))
            return;

        while (true) {
            int left_x = tx - dist;
            int right_x = tx + dist;
            int up_y = ty - dist;
            int low_y = ty + dist;


            if (left_x < 0) {
                left_x = 0;
                left_limit = true;
            }
            if (right_x >= width) {
                right_x = width - 1;
                right_limit = true;
            }

            if (up_y < 0) {
                up_y = 0;
                upper_limit = true;
            }
            if (low_y >= height) {
                low_y = height - 1;
                lower_limit = true;
            }

            if (!upper_limit)
                for (int run_x = left_x; run_x <= right_x; run_x++) {
                    if (chunks[run_x, up_y].search_entities (search_func))
                        return;
                }

            if (!lower_limit)
                for (int run_x = left_x; run_x <= right_x; run_x++) {
                    if (chunks[run_x, low_y].search_entities (search_func))
                        return;
                }

            if (!left_limit)
                for (int run_y = up_y; run_y <= low_y; run_y++) {
                    if (chunks[left_x, run_y].search_entities (search_func))
                        return;
                }

            if (!right_limit)
                for (int run_y = up_y; run_y <= low_y; run_y++) {
                    if (chunks[left_x, run_y].search_entities (search_func))
                        return;
                }
            if (upper_limit && lower_limit && left_limit && right_limit)
                return;
            dist++;
            if (max_dist != -1 && max_dist >= dist)
                return;
        }
    }

    public void foreach_in_float_rect (Func<Entity> func, FloatRect rect) {
        int left = float_to_chunk (rect.x);
        int right = float_to_chunk (rect.x + rect.width);
        int up = float_to_chunk (rect.y);
        int down = float_to_chunk (rect.y + rect.height);

        if (left < 0)
            left = 0;

        if (right >= width)
            right = width - 1;

        if (up < 0)
            up = 0;

        if (down >= height)
            down = height - 1;


        for (int x = left; x <= right; x++) {
            for (int y = up; y <= down; y++) {
                chunks[x,y].@foreach (func);
            }
        }

    }

    public void add_to_chunk (Entity e, int x, int y) {
        chunks[x,y].add (e);
    }


    public void remove_from_chunk (Entity e, int x, int y) {
        chunks[x,y].remove (e);
    }

    public static int tile_to_chunk (int i) {
        return i/16;
    }

    public static int float_to_chunk (float i) {
        return tile_to_chunk (Tile.to_tile_coords (i));
    }
}

public class Chunk {

    List<Entity> entities;

    public Chunk () {
        entities = new List<Entity> ();
    }

    public bool search_entities (SearchFunc<Entity> search_func) {
        lock (entities) {
            foreach (Entity e in entities) {
                if (search_func (e))
                    return true;
            }
            return false;
        }
    }

    public void add (Entity e) {
        lock (entities) {
            entities.insert_sorted_with_data (e, (a,b) => {
                return a.y > b.y ? 1 : -1;
            });
        }
    }

    public void remove (Entity e) {
        lock (entities) {
            entities.remove (e);
        }
    }

    public void @foreach (Func<Entity> func) {
        lock (entities) {
            entities.@foreach (func);
        }
    }
}
