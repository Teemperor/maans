using SFML.Graphics;
using SFML.System;

public struct Tile {
    public static const int SIZE = 32;

    public TileData data { get; private set; }

    public Tile.from_tile (Tile tile) {
        data = tile.data;
    }

    public Tile (string name) {
        data = (TileData) TileData.manager.get_data (name);
        assert (data != null);
    }

    public unowned Sprite get_sprite () {
        return data.sprite;
    }

    public void eval (string env) {
        var tmp = TileData.from_env ( data.base_type + "," + env);
        if (tmp != null)
            data = tmp;
    }

    public static int to_tile_coords (float x) {
        return (int) (x / 32);
    }

    public static Vector2i vector2i_to_tile_coords (Vector2f v) {
        return {to_tile_coords (v.x), to_tile_coords (v.y)};
    }

    public static float to_world_coords (int x) {
        return x * SIZE + SIZE / 2;
    }
}
