using SFML.Graphics;

public class Light {

    public static Sprite* sprite;

    public static void init () {
        sprite = Loader.get_instance ().sprite ("light", "data/light/128.png");
        sprite->origin = {64, 64};
    }

}