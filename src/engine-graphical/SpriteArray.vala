#if !SERVER

using SFML.Graphics;

public class SpriteArray {

    int sprite_index = 0;

    float current_time = 0;
    float max_time;

    Sprite*[] sprites;

    public SpriteArray (float max_time, Sprite*[] sprites) {
        this.sprites = sprites;
        this.max_time = max_time;
    }

    public Sprite* get_current_sprite () {
        return sprites[sprite_index];
    }

    public void add_time (float dt) {
        current_time += dt;
        if (current_time >= max_time) {
            current_time -= max_time;
            sprite_index++;
            check_bounds ();
        }
    }

    public void select_next_sprite () {
        sprite_index++;
        check_bounds ();
    }

    private void check_bounds () {
        sprite_index %= sprites.length;
    }

    public void reset () {
        sprite_index = 0;
    }

}

public class SpriteArrayTemplate {

    Sprite*[] sprites;
    float max_time;

    public SpriteArrayTemplate (float max_time, Sprite*[] sprites = {}) {
        this.max_time = max_time;
        this.sprites = sprites;
    }

    public SpriteArrayTemplate.from_config (ConfigFile config, string sprite_domain, 
                        string prefix, bool single_origin = false) {
        sprites = {};

        /*SFML.System.Vector2f origin = {0, 0};
        max_time = 0.1f;

        if (single_origin) {
            origin = config.get_vectorf (prefix + "_origin");
        }

        for (int index = 0; index < 99999; index++) {
            string path;
            try {
                path = config.get_path (prefix + "_sprite_" + index.to_string ());
            } catch (Error e) {
                break;
            }
            if (!single_origin)
                origin = config.get_vectorf (prefix + "_origin_" + index.to_string ());

            Sprite* s = Loader.get_instance ().sprite (sprite_domain, path);
            s->origin = origin;
            sprites += s;
        } */
    }

    public SpriteArray get_instance () {
        return new SpriteArray (max_time, sprites);
    }

    public void add_sprite (Sprite* sprite) {
        sprites += sprite;
    }
}


#endif

