#if !SERVER
using SFML.Graphics;
using SFML.System;

public class Loader {

    private Loader () {
        texture_managers = new HashTable<string, SharedTextureManager> (str_hash, str_equal);
    }

    HashTable<string, SharedTextureManager> texture_managers;
    GLib.List<Image*> images = new GLib.List<Image*> ();

    public Sprite* sprite(string domain, string path, bool smooth = false, Vector2f? origin = null) {
        Image* image = new Image.from_file (path);
        images.append (image);
        Sprite* tsprite = get_texture_manager (domain).create_sprite (image, smooth);

        if(origin != null)
            tsprite->origin = origin;
        return tsprite;
    }

    private SharedTextureManager get_texture_manager (string domain) {
        var result = texture_managers.get (domain);

        if (result == null) {
            result = new SharedTextureManager ();
            texture_managers.set (domain, result);
        }

        return result;
    }

    public void update () {
        texture_managers.foreach ((key, manager) => {
            manager.update ();
        });
    }

    public void clean () {
        foreach (Image* img in images) {
            delete img;
        }
        texture_managers.foreach ((key, manager) => {
            manager.clean ();
        });
    }

    private static Loader instance = null;

    public static Loader get_instance () {
        if(instance == null) {
            instance = new Loader();
        }
        return instance;
    }

}

#endif

