#if !SERVER
using SFML.System;
using SFML.Graphics;

public class PackNode {

    PackNode node1 = null;
    PackNode node2 = null;

    int x;
    int y;
    int w;
    int h;

    public PackNode (int x, int y, int w, int h) {
        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h;
    }

    public void far_right_down (ref Vector2i highest) {
        if (is_free ()) {
            int right = x;
            if (right > highest.x) {
                highest.x = right;
            }
            int down = y;
            if (down > highest.y) {
                highest.y = down;
            }
        }
        else {
            node1.far_right_down (ref highest);
            node2.far_right_down (ref highest);
        }
    }

    public bool pack_rect (Vector2i size, ref Vector2i pos) {
        if (is_free ()) {
            if (w < size.x || h < size.y)
                return false;
            node1 = new PackNode (x + size.x, y, w - size.x, size.y);
            node2 = new PackNode (x, y + size.y, w, h - size.y);
            pos.x = x;
            pos.y = y;
            return true;
        } else {
            bool result = node1.pack_rect (size, ref pos);
            if (!result)
                result = node2.pack_rect (size, ref pos);
            return result;
        }
    }

    public bool is_free () {
        return node1 == null;
    }
}

public class RectanglePackager {
    int width;
    int height;

    PackNode node;

    public RectanglePackager (int width, int height) {
        this.width = width;
        this.height = height;
        node = new PackNode (0, 0, width, height);
    }

    public bool pack_rectangle (Vector2i size, ref Vector2i result) {
        return node.pack_rect (size, ref result);
    }

    public Vector2i used_area () {
        Vector2i result = {1, 1};
        node.far_right_down (ref result);
        return result;
    }

}

#endif

