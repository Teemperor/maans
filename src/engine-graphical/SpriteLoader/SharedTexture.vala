#if !SERVER
using SFML.Graphics;
using SFML.System;


public class SpriteBundle {

    public Sprite* sprite;
    public IntRect rect;
    public Image* image;

    public SpriteBundle (Sprite* sprite, IntRect rect, Image* image) {
        this.sprite = sprite;
        this.rect = rect;
        this.image = image;
    }

}

public class SharedTexture {

    Image* image = null;
    Texture* texture = null;

    int size;

    List<SpriteBundle> bundles;

    RectanglePackager manager;

    bool smooth;

    public class SharedTexture (bool smooth) {
        this.smooth = smooth;
        size = (int) Texture.get_max_size ();

        manager = new RectanglePackager (size, size);

        texture = new Texture(1, 1);
        texture->smooth = smooth;
        bundles = new List<SpriteBundle>();
    }

    public bool try_to_place_sprite (Image img, Sprite* sprite) {
        Vector2i pos = {0, 0};
        bool success = manager.pack_rectangle ({(int) img.size.x, (int) img.size.y}, ref pos);
        if (success) {
            IntRect sprite_rect = {(int)pos.x, (int)pos.y, (int)img.size.x, (int)img.size.y };
            sprite->set_texture_full (texture, false);
            sprite->texture_rect = sprite_rect;
            SpriteBundle b = new SpriteBundle(sprite, sprite_rect, img);
            bundles.append (b);
        }
        return success;
    }

    public void update () {
        IntRect? rect = {0,0,0,0};
        Vector2i used_area = manager.used_area ();
        if (image != null)
            delete image;
        image = new Image.from_color (used_area.x, used_area.y, Color.transparent);
        foreach (var bundle in bundles) {
            image->copy (bundle.image, bundle.rect.x, bundle.rect.y, rect, true);
        }

        if (texture != null)
            delete texture;
        texture = new Texture (image->size.x, image->size.y);
        texture->smooth = smooth;
        texture->update_from_image (image);
        foreach (var bundle in bundles) {
            bundle.sprite->set_texture_full (texture, true);
            bundle.sprite->texture_rect = bundle.rect;
        }
    }

    public void clean () {
        delete image;
    }

    public void dump (string path) {

    }

}


public class SharedTextureManager {

    SharedTexture[] normal_textures;
    SharedTexture[] smooth_textures;

    public SharedTextureManager () {
        normal_textures = {};
        smooth_textures = {};
    }

    public Sprite* create_sprite (Image img, bool smooth) {
        unowned SharedTexture[] array = smooth ? smooth_textures : normal_textures;
        bool need_new_texture = true;
        Sprite* result = new Sprite ();

        foreach (var t in array) {
            if (t.try_to_place_sprite (img, result)) {
                need_new_texture = false;
                break;
            }
        }

        if (need_new_texture) {
            SharedTexture t = new SharedTexture (smooth);
            if (smooth)
                smooth_textures += t;
            else
                normal_textures += t;
            if (!t.try_to_place_sprite (img, result)) {
                error (@"Your GPU texture limit is to small for a sprite of the size $(img.size.x)|$(img.size.y). You have a limit of $(Texture.get_max_size ())");
            }

        }

        return result;
    }

    public void update () {
        foreach (var t in normal_textures) {
            t.update ();
        }
        foreach (var t in smooth_textures) {
            t.update ();
        }
    }

    public void clean () {
        foreach (var t in normal_textures) {
            t.clean ();
        }
        foreach (var t in smooth_textures) {
            t.clean ();
        }
    }

}

#endif

