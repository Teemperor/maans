#if !SERVER

using SFML.Graphics;
using SFML.Window;

public class MainWindow {

    public uint width { get; private set; }
    public uint height { get; private set; }

    private MainWindow (string[] args) {
        VideoMode vm = {800, 600, 32};
        uint window_style = 0;

        foreach (string arg in args) {
            if (arg.has_prefix ("-r=")) {
                string[] parts = arg.split ("=");
                parts = parts[1].split ("x");
                for (int i = 0; i < parts.length; i++) {
                    switch (i) {
                    case 0:
                        vm.width = int.parse (parts[i]);
                    break;
                    case 1:
                        vm.height = int.parse (parts[i]);
                    break;
                    case 2:
                        if (parts[i] == "BL") {
                            window_style = Style.NONE;
                        }
                        if (parts[i] == "FS") {
                            window_style = Style.FULLSCREEN;
                        }
                    break;
                    }
                }
            }
        }
        ContextSettings cs = create_context_settings ();

        width = vm.width;
        height = vm.height;

        _window = new RenderWindow (vm, "Maans", window_style, out cs);
        window.vertical_sync  = true;
    }

    RenderWindow _window;

    public unowned RenderWindow window { get { return _window; } }

    static MainWindow instance;

    public static MainWindow get_instance () {
        assert (instance != null);
        return instance;
    }

    public static void create (string[] args) {
        instance = new MainWindow (args);
    }

}

#endif

