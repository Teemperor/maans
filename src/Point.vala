using SFML.System;

public class Point {
    public int x;
    public int y;

    public Point (int x, int y) {
        this.x = x;
        this.y = y;
    }

    public Point.from_vector (Vector2i pos) {
        x = pos.x;
        y = pos.y;
    }

    public Point.from_world_coords (float x, float y) {
        this.x = Tile.to_tile_coords (x);
        this.y = Tile.to_tile_coords (y);
    }

    public float distance_to (Point p) {
        uint distX = (x - p.x);
        uint distY = (y - p.y);
        return (float) Math.sqrt (distX * distX + distY * distY);
    }

    public float distance_to_coords (float px, float py) {
        float distX = (Tile.to_world_coords(x) - px);
        float distY = (Tile.to_world_coords(y) - py);
        return (float) Math.sqrt (distX * distX + distY * distY);
    }

    public void to_world_coords (out float ox, out float oy) {
        ox = Tile.to_world_coords (x);
        oy = Tile.to_world_coords (y);
    }

    public string to_string () {
        return x.to_string () + "|" + y.to_string ();
    }
}
