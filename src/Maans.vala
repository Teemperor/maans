using SFML.Audio;
using SFML.System;
using SFML.Window;
using SFML.Graphics;

public class Maans {
    public static uint time { get; private set; }
    public static float float_time { get; private set; }

    public static ulong loop_counter { get; private set; }

    static GameState state;

    public static void main (string[] args) {

        MainWindow.create (args);

        //we first init the shaders as otherwise the max_texture_size of opengl fails
        ShaderCollection.init ();
        Loader.get_instance ();
        MoveMenu.init ();
        TileData.init ();
        TreeData.init ();
        HumanAnimationTemplate.init ();
        HumanData.init ();
        ItemData.init ();
        MousePointer.init ();
        Light.init ();
        LoadingState.init ();
        Loader.get_instance ().update ();

        time = 0;
        float_time = 0;
        float dt;

        FPSTimer fps = new FPSTimer ();

        Event e;

        state = new StartupLoadingState ();

        unowned RenderWindow window = MainWindow.get_instance ().window;

        Clock clock = new Clock ();

        while (window.opened ()) {
            window.clear (Color.black);

            dt = clock.time.seconds;
            time += clock.time.milliseconds;

            fps.update (dt);

            float_time += dt;
            loop_counter++;
            clock.restart ();
            state.update (dt);
            state.render (new RenderBundle(null, window, dt));
            state.next_state (ref state);
            window.display ();
            if (Keyboard.is_key_pressed (KeyCode.Q)) {
                window.close ();
            }
            while (window.poll_event (out e)) {
                if (e.type == EventType.CLOSED) {
                    window.close ();
                }
                else {
                    state.pass_event (e);
                }
            }

        }
    }
}
