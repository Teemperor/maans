using SFML.System;

public class HumanAnimationTemplate : Animations.AnimationTemplate {

    float[] equip_rotation;
    float[] equip_x;
    float[] equip_y;

    float[] shield_rotation;
    float[] shield_x;
    float[] shield_y;

    float[] offset_x;
    float[] offset_y;

    float[] speed_modifier;

    public HumanAnimationTemplate (int duration) {
        base (duration);
        equip_rotation = new float[frames];
        equip_x = new float[frames];
        equip_y = new float[frames];

        shield_rotation = new float[frames];
        shield_x = new float[frames];
        shield_y = new float[frames];

        offset_x = new float[frames];
        offset_y = new float[frames];

        speed_modifier = new float[frames];
        for (int i = 0; i < frames; i++)
            speed_modifier[i] = 1;
        
    }

    public float get_speed_modifier (int time) {
        return speed_modifier[get_frame (time)];
    }

    public float get_equip_rotation (int time) {
        return equip_rotation[get_frame (time)];
    }

    public float get_equip_x (int time) {
        return equip_x[get_frame (time)];
    }

    public float get_equip_y (int time) {
        return equip_y[get_frame (time)];
    }

    public float get_shield_rotation (int time) {
        return shield_rotation[get_frame (time)];
    }

    public float get_shield_x (int time) {
        return shield_x[get_frame (time)];
    }

    public float get_shield_y (int time) {
        return shield_y[get_frame (time)];
    }

    public float get_offset_x (int time) {
        return offset_x[get_frame (time)];
    }

    public float get_offset_y (int time) {
        return offset_y[get_frame (time)];
    }

    public override void write_value (string prop_name, int time, float value) {
        assert (!sealed);

        int frame = get_frame (time);

        switch (prop_name) {
            case "offset_x": set_offset_x (frame, value); break;
            case "offset_y": set_offset_y (frame, value); break;
            
            case "speed_modifier": set_speed_modifier (frame, value); break;

            case "equip_rotation": set_equip_rotation (frame, value); break;
            case "equip_x": set_equip_x (frame, value); break;
            case "equip_y": set_equip_y (frame, value); break;

            case "shield_rotation": set_shield_rotation (frame, value); break;
            case "shield_x": set_shield_x (frame, value); break;
            case "shield_y": set_shield_y (frame, value); break;
            default:
                warning (@"'$prop_name' was requested in $name even thought it doesn't exist");
                break;
        }
    }

    public void set_speed_modifier (int frame, float v) {
        speed_modifier[frame] = v;
    }

    public void set_equip_rotation (int frame, float v) {
        equip_rotation[frame] = v;
    }

    public void set_equip_x (int frame, float x) {
        equip_x[frame] = x;
    }

    public void set_equip_y (int frame, float y) {
        equip_y[frame] = y;
    }

    public void set_shield_rotation (int frame, float v) {
        shield_rotation[frame] = v;
    }

    public void set_shield_x (int frame, float x) {
        shield_x[frame] = x;
    }

    public void set_shield_y (int frame, float y) {
        shield_y[frame] = y;
    }

    public void set_offset_x (int frame, float x) {
        offset_x[frame] = x;
    }

    public void set_offset_y (int frame, float y) {
        offset_y[frame] = y;
    }

    public static AssetManager manager { get; private set; }

    public static void init () {
        manager = new AssetManager ("human_animation", (conf) => {
            try {
                int duration = conf.get_int ("Animation", "duration");

                var result = new HumanAnimationTemplate (duration);

                string data_file = conf.get_path ("Animation", "data_file");

                Animations.AnimationParser parser = new Animations.AnimationParser (data_file, result);
                parser.parse ();

                result.seal_animation ();
                return result;
            } catch (Error e) {
                error (e.message);
            }
        });
    }
}
