

public class HumanAnimation {

    public HumanAnimationTemplate template { get; private set; }

    int time;

    public HumanAnimation (string name) {
        var tmp = (HumanAnimationTemplate?) HumanAnimationTemplate.manager.get_data (name);
        assert (tmp != null);
        this.from_template (tmp);
    }

    public HumanAnimation.from_template (HumanAnimationTemplate template) {
        this.template = template;
        time = 0;
    }

    public float equip_rotation { get { return template.get_equip_rotation (time); } }
    public float equip_x { get { return template.get_equip_x (time); } }
    public float equip_y { get { return template.get_equip_y (time); } }

    public float shield_rotation { get { return template.get_shield_rotation (time); } }
    public float shield_x { get { return template.get_shield_x (time); } }
    public float shield_y { get { return template.get_shield_y (time); } }

    public float offset_x { get { return template.get_offset_x (time); } }
    public float offset_y { get { return template.get_offset_y (time); } }

    public float speed_modifier { get { return template.get_speed_modifier (time); } }

    public void update (float dt) {
        time += (int) (dt * 1000);
        time %= template.duration;
    }
}
