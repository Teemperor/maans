using SFML.Graphics;

public class Item {

    public ItemData data { get; private set; }

    public Item (string name) {
        data = (ItemData) ItemData.manager.get_data (name);
    }

    public Item.from_data (ItemData data) {
        this.data = data;
    }

}

public enum AttackType {
    STAB,
    SLASH,
    SLASHBOMB
}

public enum ItemType {
    ARMOR,
    EQUIP,
    SHIELD
}

public errordomain ItemError {
    INVALID_TYPE
}

public class ItemData : AssetData {
    public ItemData (Sprite* sprite, string display_name, ItemType t) {
        this.sprite = sprite;
        this.display_name = display_name;
        this.item_type = t;
    }

    public Sprite* sprite { get; private set; }
    public ItemType item_type { get; private set; }
    public string display_name { get; private set; }

    public static AssetManager manager { get; private set; }

    public static void init () {
        manager = new AssetManager ("item", (conf) => {
            try {
                Sprite* sprite = Loader.get_instance ().sprite ("human", conf.get_path ("Visual", "texture"));
                sprite->origin = conf.get_vectorf("Visual", "center");

                string display_type = conf.get_string ("Visual", "name");
                ItemType t = parse_item_type (conf.get_string ("Item", "type"));
                return new ItemData (sprite, display_type, t);
            } catch (Error e) {
                error (e.message);
            }
        });
    }

    private static ItemType parse_item_type (string name) throws ItemError {
        switch (name) {
            case "armor":
                return ItemType.ARMOR;
            case "equip":
                return ItemType.EQUIP;
            case "shield":
                return ItemType.SHIELD;
            default:
                throw new ItemError.INVALID_TYPE (@"Not a valid ItemType '$name'");
        }
    }
}
