
public abstract class Profession {

    public string name { get; private set; }
    public Human human { get; private set; }

    public Profession (Human human, string name) {
        this.name = name;
        this.human = human;
        human.profession = this;
    }

    public abstract void update ();
    
    public abstract void do_work ();

}
