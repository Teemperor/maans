
protected class Node {
    public Node? parent { get; private set;}

    public Point pos { get; private set; }

    public int x { get { return pos.x; } private set { pos.x = value; } }
    public int y { get { return pos.y; } private set { pos.y = value; } }
    private int g = 0;
    private float h;
    public float f { get; private set;}

    public Node (Node? _parent, Point p, Point goal)
    {
        parent = _parent;
        pos = p;
        if(parent != null)
            calcG ();
        calcH (goal);
        f = g + h; //the lower the f, the lower the cost of using this node in the result
    }

    private void calcG () //The way we walked so far in tiles
    {
        g = parent.g + 1;
    }

    private void calcH (Point goal)  //Distance to the goal in Tiles
    {
        h = pos.distance_to (goal);
    }
}


/**
 * List of Nodes that are already sorted by their f-value for quick get_lowest_f
 */
public class FastList {

    GLib.List<Node> nodes;

    public FastList () {
        nodes = new GLib.List<Node> ();
    }

    public void add (Node n) {
        nodes.insert_sorted (n, (a, b) => {
            if (a.f < b.f)
                return -1;
            else
                return 1;
        });
    }

    public uint length () {
        return nodes.length ();
    }

    public bool has_better_node (Node n) {
        bool result = false;
        nodes.foreach ((no) => {
            if (no.x == n.x && no.y == n.y && no.f <= n.f) {
                result = true;
                return;
            }
        });
        return result;
    }

    public Node get_least_f () {
        return nodes.first ().data;
    }

    public void remove (Node n) {
        nodes.remove (n);
    }

}

public class AStar
{

    public static int jobs_done = 0;

    public static GLib.List<Point?> find_path (Level level, Point start, Point goal) {

        Node? n = getPathInArray (level, start, goal);
        GLib.List<Point?> result = new GLib.List<Point?> ();

        while(n != null && n.parent != null) {
            result.prepend (n.parent.pos);
            n = n.parent;
        }
        jobs_done++;

        return result;
    }

    public static Node? getPathInArray (Level level, Point start, Point goal)
    {
        if (!level.is_passable_point (start) || !level.is_passable_point (goal)) {
            return null;
        }

        FastList openList = new FastList();  //Ways to go
        FastList closedList = new FastList();  //Already checked

        Node stn = new Node (null, new Point (start.x, start.y), new Point (start.x, start.y));
        openList.add (stn);

        while (openList.length () != 0) {
            Node q = openList.get_least_f ();  //Node with the lowest f is the best one we have so far
            openList.remove (q);  //Remove that node that we don't use it again
            GLib.List<Node> successors = new GLib.List<Node>();  //List to store all adjacent nodes
            int qx = q.x;
            int qy = q.y;
            if (level.is_passable (qx - 1,qy)) {
                Node n = new Node (q, new Point (qx - 1, qy), goal);
                successors.append (n);
            }
            if (level.is_passable (qx, qy - 1)) {
                Node n = new Node (q, new Point (qx, qy - 1), goal);
                successors.append (n);
            }
            if (level.is_passable (qx + 1, qy)) {
                Node n = new Node (q, new Point (qx + 1, qy), goal);
                successors.append (n);
            }
            if (level.is_passable (qx, qy + 1)) {
                Node n = new Node (q, new Point (qx, qy + 1), goal);
                successors.append (n);
            }
            foreach (Node n in successors) {
                if (n.x == goal.x && n.y == goal.y)  //we reached the goal
                    return n;  //let's return the final node to make a list out of it
                if (!openList.has_better_node (n) && !closedList.has_better_node (n))
                    openList.add (n); //n looks good, let's check it later
            }
            closedList.add (q);
        }
        return null;
    }
}
