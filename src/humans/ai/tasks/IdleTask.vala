
public class IdleTask : Task {

    public IdleTask (Human human) {
        base (human);
    }

    public override bool update () {
        human.play_animation (human.data.idle_animation);
        human.vx = 0;
        human.vy = 0;
        return false;
    }

}
