
public class LogTask : Task {

    Tree tree;

    public LogTask (Human human, Tree tree) {
        base (human);
        this.tree = tree;
    }

    public override bool update () {
        human.play_animation (human.data.log_animation);
        human.vx = 0;
        human.vy = 0;
        return false;
    }

}
