
public abstract class Task : Object {

    public signal void on_finish ();

    public Human human { get; private set; }

    bool _finished;
    public bool finished {
        get { return _finished; }
        set {
            if (value) {
                if (!_finished)
                    on_finish ();
                _finished = true;
            }
        }
    }

    public Task (Human h) {
        human = h;
    }

    /**
     * @return true if and only if the task has finished.
     */
    public abstract bool update ();

    public void deploy () {
        human.task = this;
    }
}
