

public class WalkToTask : Task {

    public List<Point?> to_walk_list = new List<Point?> ();
    Point target;

    public WalkToTask (Human h, float target_x, float target_y) {
        base (h);
        this.target = new Point.from_world_coords (target_x, target_y);
        calculate_path ();
    }

    public override bool update () {
        human.play_animation (human.data.walk_animation);

        finished = to_walk_list.length () == 0;
        if (!finished) {
            float tx, ty;
            Point target = to_walk_list.nth_data (0);
            target.to_world_coords (out tx, out ty);
            human.turn_to_point (tx, ty);
            human.walk_forward ();
            if (target.distance_to_coords (human.x, human.y) < Tile.SIZE / 8) {
                to_walk_list.remove (target);
            }
        }
        return finished;
    }

    private void calculate_path () {
        to_walk_list = AStar.find_path (human.level,
            new Point.from_world_coords (human.x, human.y), target);
    }

}
