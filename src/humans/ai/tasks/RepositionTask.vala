

public class RepositionTask : Task {

    public List<Point?> to_walk_list = new List<Point?> ();
    float target_x;
    float target_y;

    public RepositionTask (Human h, float target_x, float target_y) {
        base (h);
        this.target_x = target_x;
        this.target_y = target_y;
    }

    public override bool update () {
        human.play_animation (human.data.walk_animation);
        finished = human.distance_to (target_x, target_y) < Tile.SIZE / 8;
        if (!finished) {
            human.turn_to_point (target_x, target_y);
            human.walk_forward ();
        }
        else {
            human.vx = 0;
            human.vy = 0;
        }
        return finished;
    }

}
