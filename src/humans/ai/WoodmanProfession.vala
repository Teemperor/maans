
public class WoodmanProfession : Profession {

    Tree target_tree = null;

    Status status = Status.SEARCHING_TREE;

    enum Status {
        SEARCHING_TREE,
        FOUND_TREE,
        WALK_TO_TREE,
        GET_IN_POSITION,
        FELLING_TREE,
        SLABBING_TREE,
        RETURNING_PLANKS
    }

    public WoodmanProfession (Human human) {
        base (human, "woodman");
    }

    public override void update () {
        switch (status) {
            case Status.SEARCHING_TREE:
                //DO NOTHING
            break;
            case Status.FOUND_TREE:
                new WalkToTask (human, target_tree.x, target_tree.y).deploy ();
                status = Status.WALK_TO_TREE;
            break;
            case Status.WALK_TO_TREE:
                if (human.is_idling ()) {
                    target_tree.shake ();
                    status = Status.GET_IN_POSITION;
                }
            break;
            case Status.GET_IN_POSITION:
                var task = new RepositionTask (human, target_tree.x + 35.0f, target_tree.y);
                task.deploy ();
                task.on_finish.connect (() => {
                    status = Status.FELLING_TREE;
                });

            break;
            case Status.FELLING_TREE:
                new LogTask (human, target_tree).deploy ();
            break;
        }

    }

    public override void do_work () {
        switch (status) {
            case Status.SEARCHING_TREE:
                human.level.grid.search_entities_from_entity (human, (e) => {
                    if (e is Tree) {
                        target_tree = (e as Tree);
                        return true;
                    }
                    return false;
                });
                //TODO Dont crash here
                assert (target_tree != null);
                status = Status.FOUND_TREE;
            break;
        }
    }

}
