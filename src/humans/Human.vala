using SFML.Graphics;

public class Human : Entity {

    public HumanData data { get; private set; }

    float direction = 0;

    public Entity? target_entity { get; set; default = null; }

    Item equip_item = new Item ("iron_axe");
    Item shield_item = null;
    Item armor_item = new Item ("iron_helmet");

    HumanAnimation animation;

    public bool selected { get; set; }

    public Profession profession { get; set; }
    public Task task { get; set; }

    public Human (Level l, string type, float x, float y) {
        base (l, x, y);
        data = (HumanData) HumanData.manager.get_data (type);
        assert (data != null);
        start_idle ();
        profession = new WoodmanProfession (this);

        animation = new HumanAnimation.from_template (data.walk_animation);
    }

    public override void update (float dt) {
        base->update (dt);

        profession.update ();
        profession.do_work ();
        animation.update (dt);

        if (task.update ())
            start_idle ();
    }

    public override void render (RenderBundle bundle) {
        float render_x = x + animation.offset_x;
        float render_y = y + animation.offset_y;

        unowned Sprite s = data.sprite;
        s.position = {render_x, render_y};
        bundle.texture->draw_sprite (s);
        unowned Sprite shadow = data.shadow_sprite;
        shadow.position  = {render_x, y};
        bundle.texture->draw_sprite (shadow);
        if (selected) {
            unowned Sprite selected = data.selected_sprite;
            selected.position  = {render_x, y};
            bundle.texture->draw_sprite (selected);
        }

        if (armor_item != null) {
            armor_item.data.sprite->position = {render_x + data.armor_offset.x, render_y + data.armor_offset.y};
            bundle.texture->draw_sprite (armor_item.data.sprite);
        }

        if (equip_item != null) {
            equip_item.data.sprite->rotation = animation.equip_rotation;
            equip_item.data.sprite->position = {render_x + data.equip_offset.x + animation.equip_x,
                                                render_y + data.equip_offset.y + animation.equip_y};
            bundle.texture->draw_sprite (equip_item.data.sprite);
        }

        if (shield_item != null) {
            shield_item.data.sprite->rotation = animation.shield_rotation;
            shield_item.data.sprite->position = {render_x + data.shield_offset.x + animation.shield_x,
                                                 render_y + data.shield_offset.y + animation.shield_y};
            bundle.texture->draw_sprite (shield_item.data.sprite);
        }
    }

    public void play_animation (HumanAnimationTemplate template) {
        if (animation.template != template)
            animation = new HumanAnimation.from_template (template);
    }

    public void walk_forward () {
        vx = 49.9f * (float) Math.cos (direction) * animation.speed_modifier;
        vy = 49.9f * (float) Math.sin (direction) * animation.speed_modifier;
    }

    public void start_idle () {
        new IdleTask (this).deploy ();
    }

    public bool is_idling () {
        return task is IdleTask;
    }

    public override void render_light (RenderBundle bundle) {
        Sprite* s = Light.sprite;
        s->position = {x, y};
        bundle.texture->draw_sprite (s, null);
    }

    public void turn_to_point (float tx, float ty) {
        direction = Math.atan2f (ty - y, tx - x);
    }
}
