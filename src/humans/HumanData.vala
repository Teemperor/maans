using SFML.Graphics;
using SFML.System;


public class HumanData : AssetData {
    public Sprite* sprite { get; private set; }
    public Sprite* shadow_sprite { get; private set; }
    public Sprite* selected_sprite { get; private set; }

    public string display_name { get; private set; }

    public Vector2i equip_offset { get; private set; }
    public Vector2i shield_offset { get; private set; }
    public Vector2i armor_offset { get; private set; }

    public HumanAnimationTemplate log_animation { get; private set; }
    public HumanAnimationTemplate walk_animation { get; private set; }
    public HumanAnimationTemplate idle_animation { get; private set; }

    public static AssetManager manager { get; private set; }

    private static HumanAnimationTemplate get_animation (ConfigFile conf, string name,
                string domain = "Animations") {
        return (HumanAnimationTemplate?) HumanAnimationTemplate.manager.get_data
                (conf.get_string (domain, name));
    }

    private static AssetIdentifier get_dep (ConfigFile conf, string name,
                string domain = "Animations") {
        return new AssetIdentifier ("human_animation",
                conf.get_string (domain, name));
    }

    public static void init () {
        manager = new AssetManager ("maans", (conf) => {
            try {
                var result = new HumanData ();

                result.sprite = Loader.get_instance ().sprite ("human",
                        conf.get_path ("Body", "texture"));
                result.sprite->origin = conf.get_vectorf ("Body", "center");

                result.shadow_sprite = Loader.get_instance ().sprite ("human",
                        conf.get_path ("Shadow", "texture"));
                result.shadow_sprite->origin = conf.get_vectorf ("Shadow", "center");

                result.selected_sprite = Loader.get_instance ().sprite ("human",
                        conf.get_path ("Selected", "texture"));
                result.selected_sprite->origin = conf.get_vectorf ("Selected", "center");

                result.display_name = conf.get_string ("Display", "name");


                result.log_animation = get_animation (conf, "log_animation");
                result.walk_animation = get_animation (conf, "walk_animation");
                result.idle_animation = get_animation (conf, "idle_animation");


                result.equip_offset = conf.get_vectori ("Equip", "offset");
                result.shield_offset = conf.get_vectori ("Shield", "offset");
                result.armor_offset = conf.get_vectori ("Armor", "offset");

                return result;
            } catch (Error e) {
                error (e.message);
            }
        }, (conf) => {
            AssetIdentifier[] new_deps = {};

            new_deps += get_dep (conf, "log_animation");
            new_deps += get_dep (conf, "walk_animation");
            new_deps += get_dep (conf, "idle_animation");

            conf.add_dependencies (new_deps);
        });
    }
}
