using SFML.Graphics;

public class MainMenuState : GameState {

    public override void update (float dt) {

    }

    public override void render (RenderBundle bundle) {
    }

    public override void next_state (ref GameState state) {

    }
}


public class MainMenuLoadingState : LoadingState {

    public override void load_step () {
        finish_to_state (new MainMenuState ());
    }

}
