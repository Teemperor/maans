using SFML.Graphics;

public class ShaderCollection {

    static const string light_shader_code =
    """
    uniform sampler2D texture;
    uniform sampler2D light_map;
    uniform float daylight;

    void main()
    {
        // lookup the pixel in the texture
        vec4 pixel = texture2D(texture, gl_TexCoord[0].xy);
        vec4 light = texture2D(light_map, gl_TexCoord[0].xy);
        gl_FragColor = vec4 (pixel[0], pixel[1], pixel[2], daylight + light[3] * (1.0 - daylight));
    }
    """;

    static const string water_shader_code =
    """
    uniform float itime;
    uniform float offx;
    uniform float offy;

    void main(void)
    {
       float time = itime * 6.0;
       float x = gl_FragCoord.x + offx;
       float y = gl_FragCoord.y + offy;
       float mov0 = x+y+cos(sin(time)*2.)*100.+sin(x/100.)*1000.;
       float mov2 = x / 0.5;
       float c2 = abs(sin(abs(sin(y+2.0*time)/2.+mov2/2.-mov2+time)/4.
                    +sin(mov0/900.+time)+sin(y/19.+time)+sin((x+y)/44.)*3.))/1.5;
       gl_FragColor = vec4(0.1,c2,0.98,1.0);
    }

    """;

    public static Shader water_shader;
    public static Shader light_shader;

    public static void init () {
        water_shader = new Shader.from_memory (null, water_shader_code);
        light_shader = new Shader.from_memory (null, light_shader_code);
    }

}
