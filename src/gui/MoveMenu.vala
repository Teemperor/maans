using SFML.Graphics;
using SFML.Window;
using SFML.System;

public enum ActionType { WALK, GUARD, BUILD, ATTACK, NONE }

public class MoveMenu : UIElement {

    static Sprite* background;
    static Sprite* glow;
    static Sprite*[] icons;
    static int distance = 50;
    static float max_dist;
    static float min_dist;

    Vector2f mouse_pos;

    public Vector2f target_position { get; private set; }

    ActionType[] actions;

    public ActionType selected { get; private set; }

    bool visible = true;

    float percent_visible = 0.0f;
    float per_angle = 0.0f;

    public MoveMenu (UserInterface ui, Game game, Vector2f mouse_pos, ActionType[] actions) {
        base (ui, game);
        this.mouse_pos = mouse_pos;
        this.actions = actions;
        this.target_position = ui.to_world_coords ({(int) mouse_pos.x, (int) mouse_pos.y});

        selected = ActionType.NONE;
    }

    public override void render (RenderWindow window) {
        //TODO make FPS independent
        if (visible) {
            percent_visible += (1 - percent_visible) * 0.3f;
        } else {
            percent_visible += - percent_visible * 0.2f;
        }

        Color color = {255, 255, 255, (uint8) (255*percent_visible)};
        float anim_distance = distance * percent_visible;
        per_angle = 2*3.14f / actions.length;

        float anim_per_angle = per_angle * percent_visible;

        for(int i = 0; i < actions.length; i++) {
            float angle = i * anim_per_angle;

            Sprite* s = icons[i];
            s->position = {mouse_pos.x + (float) Math.cos (angle) * anim_distance,
                           mouse_pos.y + (float) Math.sin (angle) * anim_distance};
            background->position = s->position;
            background->color = color;

            s->color = color;
            s->rotation = 360 * percent_visible;

            float glow_scale = (float) (UserInterface.SCALE + 0.02 * (1.0f + Math.sin (Maans.time / 120.0f)));
            glow->scale = {glow_scale, glow_scale};
            glow->color = color;
            glow->position = s->position;


            if (i == selected)
                window.draw_sprite (glow, null);
            window.draw_sprite (background, null);
            window.draw_sprite (s, null);
        }
    }

    public override bool dispensable () {
        return !visible;
    }

    public override void pass_event (Event e) {

        if(e.type == EventType.MOUSE_BUTTON_PRESSED) {
            visible = false;
            if (e.mouse_button.button == 0)
                ui.do_action (this, selected);
        }
        else if (e.type == EventType.MOUSE_MOVED) {
            int x = MousePointer.get_instance ().x;
            int y = MousePointer.get_instance ().y;
            float dist = Tools.distance_between (x, y, mouse_pos.x, mouse_pos.y);
            if (dist > max_dist + 10) {
                visible = false;
            }
            if (visible) {
                if (dist > min_dist && dist < max_dist) {
                    double angle = Math.atan2 (y - mouse_pos.y, x - mouse_pos.x);
                    angle += per_angle * 0.5f;
                    if (angle < 0)
                        angle += 2 * Math.PI;

                    selected = (ActionType) ((angle) / per_angle);
                } else {
                    selected = ActionType.NONE;
                }
            }
        }
    }

    public static void init () {
        icons = {};
        string[] icon_names = {"walk.png", "guard.png", "build.png", "attack.png"};
        string path_prefix = "data/ui/movemenu/";
        background = Loader.get_instance ().sprite ("menu", path_prefix + "background.png", true);
        background->origin = {background->local_bounds.width/2, background->local_bounds.height/2};
        background->do_scale ({UserInterface.SCALE, UserInterface.SCALE});

        min_dist = distance - UserInterface.SCALE*background->local_bounds.width/2;
        max_dist = distance + UserInterface.SCALE*background->local_bounds.width/2;

        glow = Loader.get_instance ().sprite ("menu", path_prefix + "glow.png", true);
        glow->origin = {glow->local_bounds.width/2, glow->local_bounds.height/2};
        glow->do_scale ({UserInterface.SCALE, UserInterface.SCALE});

        for (int i = 0; i < icon_names.length; i++) {
            Sprite* s = Loader.get_instance ().sprite ("menu", path_prefix + icon_names[i], true);
            s->origin = {s->local_bounds.width/2, s->local_bounds.height/2};
            s->do_scale ({UserInterface.SCALE, UserInterface.SCALE});
            icons += s;
        }
    }
}
