using SFML.Graphics;
using SFML.Window;
using SFML.System;


public abstract class UIElement {

    protected Game game;
    protected UserInterface ui;

    public UIElement (UserInterface ui, Game game) {
        this.game = game;
        this.ui = ui;
    }

    public abstract bool dispensable ();

    public abstract void render (RenderWindow window);

    public abstract void pass_event (Event e);
}

public class UserInterface {
    public static float SCALE = 0.5f;

    public static int camera_move_speed { get; set; }

    Game game;

    float move_x = 0;
    float move_y = 0;
    float zoom_delta = 0;

    unowned RenderWindow tmp_window;

    public InterfaceSelection current_selection { get; set; }

    List<UIElement> elements = new List<UIElement> ();

    public UserInterface (Game game) {
        this.game = game;
        camera_move_speed = 14;
        elements.append (new InterfaceSelection (this, game));
    }

    public void update (float dt) {

        zoom_delta *= 0.93f;
        game.level_renderer.view.zoom (1 + zoom_delta);

        var x = MousePointer.get_instance ().x;
        var y = MousePointer.get_instance ().y;
        int dx = 0;
        int dy = 0;

        if (x < 4)
            dx -= camera_move_speed;

        if (y < 4)
            dy -= camera_move_speed;

        if (x >= MousePointer.get_instance ().max_x - 1)
            dx += camera_move_speed;

        if (y >= MousePointer.get_instance ().max_y - 1)
            dy += camera_move_speed;

        if (dx != 0)
            move_x = dx;

        if (dy != 0)
            move_y = dy;

        move_y *= 0.93f;
        move_x *= 0.93f;

        var limited = game.level_renderer.move_camera ((int) move_x, (int) move_y);

        if (limited.x_limited)
            move_x = 0;


        if (limited.y_limited)
            move_y = 0;

    }

    public void render (RenderBundle bundle) {
        tmp_window = bundle.window;
        unowned View view_bak = bundle.window->view;
        bundle.window->view = bundle.window->default_view;
        foreach (var ele in elements) {
            ele.render (bundle.window);
            if (ele.dispensable ()) {
                elements.remove (ele);
            }
        }
        bundle.window->view = view_bak;
    }

    public void do_action (MoveMenu menu, ActionType action) {
        switch (action) {
            case ActionType.WALK:
                if (current_selection == null)
                    return;
                foreach (Entity e in current_selection.get_entities ()) {
                    if (e is Human) {
                        var e_human = e as Human;
                        new WalkToTask (e_human, menu.target_position.x,menu.target_position.y).deploy ();
                    }

                }
                break;
            default:
                break;
        }
    }

    public void pass_event (Event e) {
        foreach (var ele in elements) {
            ele.pass_event (e);
        }

        if (e.type == EventType.MOUSE_BUTTON_PRESSED) {
            MouseButtonEvent m = e.mouse_button;
            var x = MousePointer.get_instance ().x;
            var y = MousePointer.get_instance ().y;
            if (m.button == 1) {
                elements.append (new MoveMenu (this, game, {x, y}, {ActionType.WALK, ActionType.GUARD, ActionType.BUILD}));
            }
        }
        else if (e.type == EventType.MOUSE_WHEEL_MOVED) {
            MouseWheelEvent m = e.mouse_wheel;

            zoom_delta = -m.delta/30.0f;
        }
    }

    public Vector2f to_world_coords (Vector2i coords) {
        return tmp_window.map_pixel_to_coords (coords, game.level_renderer.view);
    }

}
