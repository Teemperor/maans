using SFML.Graphics;
using SFML.Window;
using SFML.System;

public class InterfaceSelection : UIElement{
    RectangleShape rect;
    bool started = false;

    Color background;

    Entity[] entities = {};

    public InterfaceSelection (UserInterface ui, Game game) {
        base (ui, game);
        background = {255, 255, 255, 60};
        rect = new RectangleShape ();
        rect.position = {50, 50};
        rect.size = {0, 0};
        rect.outline_color = Color.white;
        rect.fill_color = background;
        rect.outline_thickness = 1;
        ui.current_selection = this;
    }

    public unowned Entity[] get_entities () {
        return entities;
    }

    public override void render (RenderWindow window) {
        if (started)
            window.draw_rectangle_shape (rect, null);
    }

    public override bool dispensable () {
        return false;
    }

    public override void pass_event (Event e) {
        if(e.type == EventType.MOUSE_BUTTON_PRESSED) {
            MouseButtonEvent m = e.mouse_button;
            int x = MousePointer.get_instance ().x;
            int y = MousePointer.get_instance ().y;
            if(m.button == 0) {
                started = true;
                rect.position = {x, y};
                rect.size = {0, 0};
            }
        }
        else if (e.type == EventType.MOUSE_MOVED) {
            int x = MousePointer.get_instance ().x;
            int y = MousePointer.get_instance ().y;
            var pos = rect.position;
            rect.size = {x - pos.x, y - pos.y};
        }
        else if (e.type == EventType.MOUSE_BUTTON_RELEASED) {
            MouseButtonEvent m = e.mouse_button;
            if(m.button == 0) {
                started = false;
                change_selection (false);
                entities = {};
                Vector2f coords = ui.to_world_coords ({(int)rect.position.x, (int)rect.position.y});
                Vector2f size = ui.to_world_coords ({(int) (rect.position.x + rect.size.x), (int) (rect.position.y + rect.size.y)});
                size.x -= coords.x;
                size.y -= coords.y;
                FloatRect select_rect = {coords.x, coords.y,
                    size.x, size.y };
                select_rect = Tools.redo_rect (select_rect);
                game.level.grid.foreach_in_float_rect (((e) => {
                    if (e.in_rect (select_rect)) {
                        entities += e;
                    }
                }), select_rect);
                change_selection (true);
            }
        }
    }

    private void change_selection (bool val) {
        foreach (Entity e in entities) {
            if (e is Human) {
                (e as Human).selected = val;
            }
        }
    }

}
