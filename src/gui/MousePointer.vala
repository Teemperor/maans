using SFML.Graphics;
using SFML.Window;
using SFML.System;

public class MousePointer {

    public int x { get; private set; }
    public int y { get; private set; }

    public int max_x { get; private set; }
    public int max_y { get; private set; }

    static Sprite* normal_cursor_sprite;

    public MousePointer () {
        update (0);
        MainWindow.get_instance ().window.mousecursor_visible = false;
    }

    public void render (RenderBundle bundle) {
        normal_cursor_sprite->position = {x, y};
        bundle.window->draw_sprite (normal_cursor_sprite, null);
    }

    public void update (float dt) {
        var main_window = MainWindow.get_instance ();
        unowned RenderWindow window = main_window.window;
        var mpos = Mouse.get_position (window);

        max_x = (int) main_window.width;
        max_y = (int) main_window.height;

        Vector2i rel_pos = {mpos.x - max_x/2, mpos.y - max_y/2};
        Mouse.set_position ({max_x/2, max_y/2}, window);

        x += rel_pos.x;
        y += rel_pos.y;

        if (x < 0)
            x = 0;
        if (y < 0)
            y = 0;
        if (x >= max_x)
            x = max_x - 1;
        if (y >= max_y)
            y = max_y - 1;
    }

    static MousePointer instance = null;

    public static MousePointer get_instance () {
        if (instance == null)
            instance = new MousePointer ();
        return instance;
    }

    public static void init () {
        normal_cursor_sprite = Loader.get_instance ().sprite ("cursors", "data/ui/default_cursor.png");
    }

}
