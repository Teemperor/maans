using SFML.Graphics;


public class MiscObject : Entity {

    static List<Sprite*> sprites = new List<Sprite*> ();

    int type;

    public MiscObject (Level level, float x, float y, int type) {
        base (level, x, y);
        this.type = type;
    }

    public override void update (float dt) {

    }

    public override void render (RenderBundle bundle) {
        unowned Sprite s = sprites.nth_data (type);
        s.position = {x, y};
        bundle.window->draw_sprite (s, null);
    }


    public override void render_light (RenderBundle tex) {

    }

    public static void init (ConfigFile conf) throws Error {
        Sprite* sprite = Loader.get_instance ().sprite ("human", conf.get_string ("Visual", "texture"));
        sprite->origin = conf.get_vectorf ("Visual", "center");
        sprites.append (sprite);
    }

}
