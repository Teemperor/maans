using SFML.Graphics;
using SFML.System;

public class Tree : Entity {

    TreeData data;

    float shake_timer = 0;
    float max_shake_timer = 1;
    float fall_degree = 0.0f;

    public int8 fallen { get; private set; }

    public Tree (string name, Level level, float x, float y) {
        base (level, x, y);
        data = (TreeData) TreeData.manager.get_data (name);
        fallen = 0;
    }

    public override void update (float dt) {
        shake_timer -= dt;
        if (shake_timer < 0)
            shake_timer = 0;
    }

    public override void render (RenderBundle bundle) {
        float sway = 0.5f * level.weather.wind_force * (float) (1.0f + Math.sin (Maans.time / 220.0f));

        if (fallen != 0 && Math.fabs (fall_degree) < 90.0f) {
            fall_degree += 90.0f * bundle.dt * fallen;
        }

        var d_sway = shake_timer/max_shake_timer * 9.0f * (float) Math.cos (shake_timer);
        sway += d_sway;

        unowned Sprite s = data.shadow_sprite;
        s.position = {x, y};

        int raw_shadow_alpha = (int) (255 - Math.fabsf (fall_degree/90.0f) * 255.0f);

        if(raw_shadow_alpha < 0)
            raw_shadow_alpha = 0;
        if(raw_shadow_alpha > 255)
            raw_shadow_alpha = 255;
        uint8 shadow_alpha = (uint8) raw_shadow_alpha;

        s.color = {0, 0, 0, shadow_alpha};
        bundle.texture->draw_sprite (s, null);

        s = data.botsprite;
        s.rotation = fall_degree;
        s.position = {x, y};
        bundle.texture->draw_sprite (s, null);

        float last_x = x + Tools.sin_degree (fall_degree) * data.bot_off.y;
        float last_y = y - Tools.cos_degree (fall_degree) * data.bot_off.y;

        s = data.midsprite;
        s.rotation = sway + fall_degree;
        s.position = {last_x, last_y};
        bundle.texture->draw_sprite (s, null);

        sway *= 2;

        s = data.topsprite;
        s.rotation = sway + fall_degree;
        s.position = {last_x + Tools.sin_degree (sway + fall_degree) * data.mid_off.y,
                      last_y - Tools.cos_degree (sway + fall_degree) * data.mid_off.y};
        bundle.texture->draw_sprite (s, null);
    }

    public override void damage (uint16 dmg) {
        base.damage (dmg);
        shake ();
        if (health <= 0) {
            fallen = 1;
        }
    }

    public override void render_light (RenderBundle bundle) {
    }

    public void shake (float strength = 2.0f) {
        max_shake_timer = shake_timer = strength;
    }
}

public class TreeData : AssetData {
    public TreeData (Sprite* shadow_sprite, Sprite* topsprite, Sprite* midsprite, Sprite* botsprite, Vector2i bot_off, Vector2i mid_off) {
        this.shadow_sprite = shadow_sprite;
        this.topsprite = topsprite;
        this.midsprite = midsprite;
        this.botsprite = botsprite;
        this.bot_off = bot_off;
        this.mid_off = mid_off;
    }

    public Sprite* topsprite { get; private set; }
    public Sprite* midsprite { get; private set; }
    public Sprite* botsprite { get; private set; }
    public Sprite* shadow_sprite { get; private set; }
    public Vector2i bot_off { get; private set; }
    public Vector2i mid_off { get; private set; }

    public static AssetManager manager { get; private set; }

    public static void init () {
        manager = new AssetManager ("tree", (conf) => {
            try {
                Sprite* sprite = Loader.get_instance ().sprite ("tree",
                            conf.get_path ("Top", "texture"), false, conf.get_vectorf ("Top", "center"));
                Sprite* sprite2 = Loader.get_instance ().sprite ("tree",
                            conf.get_path ("Mid", "texture"), false, conf.get_vectorf ("Mid", "center"));
                Sprite* sprite3 = Loader.get_instance ().sprite ("tree",
                            conf.get_path ("Bot", "texture"), false, conf.get_vectorf ("Bot", "center"));

                Sprite* shadow_sprite = Loader.get_instance ().sprite ("tree",
                            conf.get_path ("Shadow", "texture"), false, conf.get_vectorf ("Shadow", "center"));

                Vector2i bot = conf.get_vectori ("Mid", "offset_to_bot");
                Vector2i mid = conf.get_vectori ("Top", "offset_to_mid");

                return new TreeData (shadow_sprite, sprite, sprite2, sprite3, bot, mid);
            } catch (Error e) {
                error (e.message);
            }
        });
    }

}
