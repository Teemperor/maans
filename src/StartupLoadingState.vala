
public class StartupLoadingState : LoadingState {


    Queue<ConfigFile> configs = new Queue<ConfigFile> ();
    bool finished_listing = false;

    ConfigFileDatabase db = new ConfigFileDatabase ();

    public override void load_async () {

        var data_node = new FSNode ("data");
        var list_configs = data_node.get_configs_in_subdirs (db);
        foreach (ConfigFile config in list_configs) {
            configs.push_tail (config);
        }
        finished_listing = true;
    }

    public override void load_step () {
        if (finished_listing) {
            var c = configs.pop_head ();
            if (c == null) {
                Loader.get_instance ().update ();
                MetaAssetManager.get_instance ().print_asset_report ();
                finish_to_state (new PlayState ());
            } else {
                message (@"Loading: " + c.filepath);
                if (!c.load ()) {
                    configs.push_tail (c);
                }
            }
        }
    }
}
