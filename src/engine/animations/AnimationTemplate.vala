
namespace Animations {

    public abstract class AnimationTemplate : AssetData {
        public int frames { get; private set; }
        public int duration { get; private set; }
        public bool sealed { get; private set; }

        public AnimationTemplate (int duration) {
            this.duration = duration;
            frames = (duration / get_frame_duration ()) + 1;
        }

        public abstract void write_value (string prop_name, int time, float value);

        public int fix_frame (int frame) {
            if (frame < 0) {
                frame = 0;
            } else if (frame >= frames) {
                frame = frames - 1;
            }
            return frame;
        }

        public virtual int get_frame_duration () {
            return 16;
        }

        public int get_frame (int time) {
            int result = time / get_frame_duration ();
            return fix_frame (result);
        }

        public void seal_animation () {
            sealed = true;
        }
    }
}
