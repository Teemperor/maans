
namespace Animations {

    public class AnimationParser {

        AnimationTemplate template;
        string filepath;

        // Regex for validating
        static Regex func_regex = null;
        static Regex assign_regex = null;

        static string func_regex_str = "[\\w]+\\[[\\d]+[-[\\d]+]?\\]=[\\w]+\\(\\-?\\d*(,\\-?\\d+)?\\)";
        static string assign_regex_str = "[\\w]+\\[[\\d]+[-[\\d]+]?\\]=\\d+";

        public AnimationParser (string filepath, AnimationTemplate template) {
            this.template = template;
            this.filepath = filepath;

            if (func_regex == null) {
                try {
                    func_regex = new Regex (func_regex_str.replace (" ", ""));
                    assign_regex = new Regex (assign_regex_str.replace (" ", ""));
                } catch (Error e) {
                    warning (e.message);
                }
            }
        }

        public void parse () {
            var file = File.new_for_path (filepath);

            if (!file.query_exists ()) {
                warning (@"File $filepath doesn't exist");
                return;
            }

            try {
                var dis = new DataInputStream (file.read ());
                string line;
                int linenum = 1;
                // Read lines until end of file (null) is reached
                while ((line = dis.read_line (null)) != null) {
                    line = line.replace (" ", "");
                    if (line.has_prefix ("#") || line.length == 0)
                        continue;

                    parse_line (linenum, line);
                    linenum ++;
                }
            } catch (Error e) {
                error ("%s", e.message);
            }
        }

        private bool parse_line (int linenum, string line) {
            // Test if the line is well-formed
            Statement stm;
            if (func_regex.match (line)) {
                stm = new FunctionStatement (line, filepath, linenum);
            } else if (assign_regex.match (line)) {
                stm = new AssignStatement (line, filepath, linenum);
            } else {
                warning (@"Line $linenum in $filepath is malformed");
                return false;
            }
            stm.parse (template);
            return true;
        }
    }
}
