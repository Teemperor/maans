
namespace Animations {

    public abstract class Statement {
        public string prop_name { get; private set; }

        public int start_time { get; private set; }
        public int end_time { get; private set; }
        public int duration { get; private set; }

        public string filepath { get; private set; }
        public int line_num { get; private set; }

        public Statement (string line, string filepath, int line_num) {
            this.filepath = filepath;
            this.line_num = line_num;
            int name_end_pos = line.index_of ("[");
            prop_name = line.substring (0, name_end_pos);
            int time_end_pos = line.index_of ("]");

            string time_str = line.substring (name_end_pos + 1, time_end_pos - name_end_pos + 1);

            if (time_str.contains ("-")) {
                int separator_index = time_str.index_of ("-");
                start_time = int.parse (time_str.substring (0, separator_index));
                end_time = int.parse (time_str.substring (separator_index + 1));
            } else {
                start_time = int.parse (time_str);
                end_time = start_time;
            }
            duration = end_time - start_time;
        }

        public abstract void parse (AnimationTemplate template);

    }

    public class FunctionStatement : Statement {

        string function_name;

        bool[] parameter_is_int = {};
        int[] int_parameters = {};
        float[] float_parameters = {};

        public FunctionStatement (string line, string filepath, int line_num) {
            base (line, filepath, line_num);
            int equal_pos = line.index_of ("=");
            int args_start_pos = line.index_of ("(");
            function_name = line.substring (equal_pos + 1, args_start_pos - equal_pos - 1);


            int last_index = args_start_pos + 1;
            for (int index = args_start_pos + 1; index < line.length; index++) {
                char c = line.get (index);
                if (c == ')' || c == ',') {
                    string value_str = line.substring (last_index, index - last_index);
                    last_index = index + 1;
                    if (value_str.contains (".")) {
                        parameter_is_int += false;
                        int_parameters += 0;
                        float_parameters += (float) double.parse (value_str);
                    } else {
                        parameter_is_int += true;
                        float_parameters += int.parse (value_str);
                        int_parameters += int.parse (value_str);
                    }
                }
            }
        }

        public override void parse (AnimationTemplate template) {
            switch (function_name) {
                case "linear":
                    func_linear (template);
                break;
                case "parabel":
                    func_parabel (template);
                break;
                default:
                    error (@"Unkown function '$function_name'");
            }
        }

        private void func_linear (AnimationTemplate template) {
            for (int i = start_time; i <= end_time; i+= template.get_frame_duration ()) {
                template.write_value (prop_name, i,
                    ((end_time - i)/((float)duration)) *
                    (float_parameters[1] - float_parameters[0]) + float_parameters[0]);
            }
        }

        private void func_parabel (AnimationTemplate template) {
            int off_x = (end_time - start_time)/2;
            float off_y = float_parameters[0];
            var a = -off_y / ((start_time - off_x) * (start_time - off_x));
            for (int i = start_time; i <= end_time; i+= template.get_frame_duration ()) {
                var val = a * (i - off_x) * (i - off_x) + off_y;
                template.write_value (prop_name, i, val);
            }
        }

    }

    public class AssignStatement : Statement {

        float assign_value;

        public AssignStatement (string line, string filepath, int line_num) {
            base (line, filepath, line_num);
            int equal_pos = line.index_of ("=");
            string value_str = line.substring (equal_pos + 1);
            assign_value = (float) double.parse (value_str);
        }

        public override void parse (AnimationTemplate template) {
            for (int i = start_time; i <= end_time; i+= template.get_frame_duration ()) {
                template.write_value (prop_name, i, assign_value);
            }
        }
    }
}
