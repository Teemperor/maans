public errordomain AssetManagerError {
    NO_SUCH_ASSETMANAGER
}

public class MetaAssetManager {

    HashTable<string, AssetManager> managers = new HashTable<string, AssetManager> (str_hash, str_equal);

    /**
     * Passes the given ConfigFile to the reponsible InitManager.
     * Throws a NO_SUCH_ASSETMANGER-Error in case there in no AssetManager
     * specified for the config_type the file specifies.
     */
    public void init_file (ConfigFile file) throws AssetManagerError {
        var m = managers.lookup (file.config_type);
        if (m != null)
            m.load (file);
        else
            throw new AssetManagerError.NO_SUCH_ASSETMANAGER (@"No AssetManager for $(file.config_type)");
    }

    public void pre_init_file (ConfigFile file) throws AssetManagerError {
        var m = managers.lookup (file.config_type);
        if (m != null)
            m.call_pre_init (file);
        else
            throw new AssetManagerError.NO_SUCH_ASSETMANAGER (@"No AssetManager for $(file.config_type)");
    }

    /**
     * Adds a AssetManager to the registry.
     */
    public void add_manager (string id, AssetManager manager) {
        managers.insert (id.down (), manager);
    }

    public AssetManager? get_manager (string id) {
        return managers.lookup (id.down ());
    }

    public void print_asset_report () {
        foreach (var m in managers.get_values ()) {
            message (@"$(m.name): Loaded $(m.assets_loaded) assets");
        }
    }

    // singleton pattern
    static MetaAssetManager instance = null;

    public static MetaAssetManager get_instance () {
        if (instance == null)
            instance = new MetaAssetManager ();
        return instance;
    }
}

public class AssetData {

    public string name { get; set; default = "INVALID_ID"; }
    public uint32 uid { get; set; default = 0; }

}

public class AssetIdentifier {

    public string id { get; private set; }
    public string asset_type { get; private set; }

    public AssetIdentifier (string id, string asset_type) {
        this.id = id;
        this.asset_type = asset_type;
    }

}

public class AssetManager {

    HashTable<string, AssetData> hash_data = new HashTable<string, AssetData> (str_hash, str_equal);
    List<AssetData> array_data = new List<AssetData>();

    uint32 last_uid = 0;

    public string name { get; private set; }

    public uint assets_loaded { get; private set; }

    public delegate AssetData AssetInitFunc (ConfigFile config);

    public delegate void PreInitFunc (ConfigFile config);

    public delegate bool ForeachAssetFunc (AssetData data);

    unowned AssetInitFunc<AssetData> ini;
    unowned PreInitFunc pre_init;

    /**
     * Creates a AssetManager and registers it at the MetaAssetManager.
     * @id identifies the Manager. 'id' is identical to the first
     * line of a configuration file and has to be unique in a project.
     * It is recommended that 'id' is just a single word without special characters.
     * The init paramter
     * @ini is a AssetInitFunc that returns a object of the template-type and
     * takes a ConfigFile which should be turned into a object of
     * template type
     */
    public AssetManager (string id, AssetInitFunc ini, PreInitFunc? pre_init = null) {
        message (@"Attaching AssetManager for '$id'");
        this.name = id;
        this.ini = ini;
        this.pre_init = pre_init;
        assets_loaded = 0;
        MetaAssetManager.get_instance ().add_manager (id, this);
    }

    public void call_pre_init (ConfigFile config) {
        if (pre_init != null)
            pre_init (config);
    }

    public void load (ConfigFile config) {
        var result = ini (config);
        var name = config.name;
        result.uid = last_uid;
        result.name = name;
        last_uid++;
        hash_data.insert (name, result);
        array_data.append (result);
        assets_loaded++;
    }

    public void foreach_asset (ForeachAssetFunc f) {
        foreach (AssetData data in array_data) {
            if (!f(data))
                return;
        }
    }

    public AssetData? get_data (string name) {
        return hash_data.lookup (name);
    }

    public AssetData? get_data_by_uid (uint32 uid) {
        if (uid >= array_data.length ())
            return null;
        return array_data.nth_data(uid);
    }
}
