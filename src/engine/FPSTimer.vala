


public class FPSTimer {

    float time = 0;
    int frames = 0;

    float fps { get; private set; }

    bool print_fps { get; private set; }

    public class FPSTimer (bool print_fps = true) {
        fps = 0;
        this.print_fps = print_fps;
    }


    public void update (float dt) {
        time += dt;
        frames++;
        if (time >= 1.0f) {
            fps = frames;
            frames = 0;
            time -= 1.0f;
            if (print_fps)
                print ("FPS: " + fps.to_string () + "\n");
        }
    }

}