using SFML.Graphics;

public class Tools {

    private Tools () {}

    public static const float PI = 3.14f;

    public static float distance_between (float x1, float y1, float x2, float y2) {
        return (float) Math.sqrt ((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
    }

    public static inline float to_rad (float a) {
        return a / 360.0f * 2.0f * PI;
    }

    public static inline float to_degree (float a) {
        return a / (2.0f * PI) * 360.0f;
    }

    public static float sin_degree (float a) {
        return Math.sinf (to_rad (a));
    }

    public static float cos_degree (float a) {
        return Math.cosf (to_rad (a));
    }

    /*
     * Takes any rect and returns a rect that specifies the same area but
     * with width and height always as positive values.
     */
    public static FloatRect redo_rect (FloatRect r) {
        if (r.width > 0 && r.height > 0) {
            return r;
        }

        FloatRect result = r;
        if (r.width < 0) {
            result.x = r.x + r.width;
            result.width = -r.width;
        }

        if (r.height < 0) {
            result.y = r.y + r.height;
            result.height = -r.height;
        }

        return result;
    }

}

