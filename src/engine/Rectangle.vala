

public struct Rectangle {

    int x1;
    int y1;
    int x2;
    int y2;

    public bool intersects (Rectangle r) {
        return x1 < r.x2 && x2 > r.x1 && y1 < r.y2 && y2 > r.y1;
    }
}
