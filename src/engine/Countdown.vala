

public class Countdown {

    uint starting_time;
    uint duration;
    Level level;

    public class Countdown.seconds (Level level, float seconds) {
        starting_time = level.time;
        duration = (uint) (seconds * 1000);
        this.level = level;
    }

    public float percent_expired () {
        return ((float) (level.time - starting_time))/duration;
    }

    public bool is_expired () {
        return level.time + duration >= starting_time;
    }

}