
public class ThreadHelper {

    public static void run_in_thread (owned ThreadFunc<void*> func) {
        run_in_thread_internal.begin (func);
    }

    private static async void run_in_thread_internal (owned ThreadFunc<void*> func) {
        new Thread<void*> ("test", func);
        yield;
    }

}
