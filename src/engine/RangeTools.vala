

public class RangeTools {

    public static int set_in_range (int v, int min, int max) {
        if (v <= min)
            return min;
        if (v >= max)
            return max;
        return v;
    }
    
    public static Rectangle fix_rectangle (Rectangle rect, int max_w, int max_h) {
        Rectangle result = {0, 0, 0, 0};
        result.x1 = rect.x1 < 0 ? 0 : rect.x1;
        result.x2 = rect.y1 < 0 ? 0 : rect.y1;
        result.x2 = rect.x2 >= max_w ? max_w - 1 : rect.x2;
        result.y2 = rect.y2 >= max_h ? max_h - 1 : rect.y2;
        return result;
    }

}