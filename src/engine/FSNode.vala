
public class FSNode {

    /**
     * True if and only if the FSNode represents a directory
     */
    bool is_dir { get; private set; }

    /**
     * The path of the File that this FSNode represents. It's not specified
     * if this variable is absolute or relative to the current working directory.
     */
    string path { get; private set; }

    public FSNode (string path) {
        this.path = path;
        is_dir = FileUtils.test (path, FileTest.IS_DIR);
    }

    public FSNode[] list_contents () {
        FSNode[] result = {};

        try {
            Dir dir = Dir.open (path, 0);
            string? name = null;
            while ((name = dir.read_name ()) != null) {
                string new_path = Path.build_filename (path, name);
                FSNode node = new FSNode (new_path);
                result += node;
            }
        } catch (FileError err) {
            warning (err.message);
        }
        return result;
    }

    /**
     * Same as @get_configs but it also checks all subdirectories.
     */
    public ConfigFile[] get_configs_in_subdirs (ConfigFileDatabase db) {
        ConfigFile[] result = get_configs (db);

        foreach (FSNode n in list_contents ()) {
            if (n.is_dir) {
                var tmp_result = n.get_configs_in_subdirs (db);
                foreach (ConfigFile c in tmp_result) {
                    result += c;
                }
            }
        }
        return result;
    }


    /**
     * Turns all files with a given name-suffix into ConfigFile-object.
     * The default suffix is '.cfg'.
     */
    public ConfigFile[] get_configs (ConfigFileDatabase db, string suffix = ".cfg") {
        FSNode[] nodes = list_contents ();

        ConfigFile[] result = {};

        foreach (FSNode n in nodes) {
            if (n.path.has_suffix (suffix) && !n.is_dir) {
                try {
                    var c = new ConfigFile.from_path (n.path, db);
                    result += c;
                } catch (Error e) {
                    warning (@"Error processing '$(n.path)': $(e.message)");
                }
            }
        }
        return result;
    }

}
