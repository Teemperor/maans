

public class ConfigFileDatabase {

    List<ConfigFile> loaded_configs = new List<ConfigFile> ();

    public ConfigFileDatabase () {

    }

    public bool is_loaded (string type, string name) {
        foreach (ConfigFile conf in loaded_configs) {
            if (conf.name == name && conf.config_type == type)
                return true;
        }
        return false;
    }

    public void add_config (ConfigFile config) {
        loaded_configs.append (config);
    }
}
