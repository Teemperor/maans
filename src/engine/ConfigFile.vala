using SFML.System;

public errordomain ConfigError {
    NO_NAME
}

public class ConfigFile {

    KeyFile keyfile;

    public string filepath { get; private set; }

    public string config_type { get; private set; }

    public string dir { get; private set; }

    public string name { get; private set; }

    List<AssetIdentifier> required_assets;

    bool needs_preinit = true;

    ConfigFileDatabase db;

    public ConfigFile.from_path (string path, ConfigFileDatabase db) throws Error {
        this.filepath = path;
        this.db = db;
        var paths = path.split (Path.DIR_SEPARATOR.to_string ());

        string[] dir_parts = {};
        for (int i = 0; i < paths.length - 1; i++) {
            dir_parts += paths[i];
        }

        this.dir = string.joinv (Path.DIR_SEPARATOR.to_string (), dir_parts);
        init ();
    }

    public ConfigFile (string path, string directory, ConfigFileDatabase db) throws Error {
        this.filepath = path;
        this.dir = directory;
        this.db = db;
        init ();
    }

    private void init () {
        message (@"Init of $filepath");
        try {
            keyfile = new KeyFile ();
            keyfile.load_from_file (filepath, KeyFileFlags.NONE);

            config_type = get_string ("General", "type");
            name = get_string ("General", "name");
            assert (!name.contains ("."));
            assert (!config_type.contains ("."));


            required_assets = new List<AssetIdentifier> ();
            if (has_key ("General", "requirements")) {
                var list = keyfile.get_string_list ("General", "requirements");
                for (int i = 0; i < list.length; i++) {
                    var str = list[i];
                    var str_list = str.split (",");
                    if (str_list.length != 2) {
                        warning (@"$str in $filepath is a contains more than 1 dot");
                    }
                    required_assets.append (new AssetIdentifier (str_list[0], str_list[1]));
                }
            }

        } catch (Error e) {
            warning (e.message);
        }
    }

    public void add_dependencies (AssetIdentifier[] assets) {
        foreach (var a in assets) {
            required_assets.append (a);
        }
    }

    /**
     * Tries to load this ConfigFile over the MetaAssetManager and adds
     * it on success to the ConfigFileDatabase.
     * @return True if the file was successfully turned into a Asset
     */
    public bool load () {
        if (needs_preinit) {
            MetaAssetManager.get_instance ().pre_init_file (this);
            needs_preinit = false;
        }

        AssetIdentifier[] to_remove = {};
        bool can_load = true;

        foreach (AssetIdentifier pair in required_assets) {
            if (db.is_loaded (pair.id, pair.asset_type)) {
                to_remove += pair;
            } else {
                can_load = false;
            }
        }

        foreach (var pair in to_remove) {
            required_assets.remove (pair);
        }

        if (can_load) {
            MetaAssetManager.get_instance ().init_file (this);
            db.add_config (this);
            return true;
        }
        else {
            return false;
        }
    }

    public bool has_key (string group, string name) {
        try {
            return keyfile.has_key (group, name);
        } catch (Error e) {
            error (e.message);
        }
    }

    public int get_int (string group, string name) throws KeyFileError {
        return keyfile.get_integer (group, name);
    }

    public bool get_bool (string group, string name) throws KeyFileError {
        return keyfile.get_boolean (group, name);
    }

    public float get_float (string group, string name) throws KeyFileError {
        return (float) keyfile.get_double (group, name);
    }

    public string get_string (string group, string name) throws KeyFileError {
        return keyfile.get_string (group, name).strip ();
    }

    public Vector2i get_vectori (string group, string name) throws KeyFileError {
        var array = keyfile.get_integer_list (group, name);
        if (array.length != 2) {
            throw new KeyFileError.PARSE (@"$group.$name has the dimension $(array.length) but 2 was expected.");
        }
        return {array[0], array[1]};
    }

    public Vector2f get_vectorf (string group, string name) throws KeyFileError {
        var array = keyfile.get_double_list (group, name);
        if (array.length != 2) {
            throw new KeyFileError.PARSE (@"Parsing error in $filepath: $group.$name has the dimension $(array.length) but 2 was expected.");
        }
        return {(float) array[0], (float) array[1]};
    }

    public string get_path (string group, string name) throws KeyFileError {
        var filename = get_string (group, name);
        return dir + Path.DIR_SEPARATOR.to_string () + filename;
    }

}
