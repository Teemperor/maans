using SFML.Graphics;
using SFML.System;

public abstract class Entity : Object {

    public float x { get; private set; }
    public float y { get; private set; }

    public float vx { get; set; }
    public float vy { get; set; }

    public Level level { get; private set; }

    private int chunk_x;
    private int chunk_y;

    public uint32 uid { get; private set; }

    static uint32 uid_counter = 0;

    public uint16 health { get; private set; }

    public Entity (Level l, float x, float y) {
        this.x = x;
        this.y = y;
        this.level = l;
        chunk_x = ChunkGrid.float_to_chunk (x);
        chunk_y = ChunkGrid.float_to_chunk (y);
        uid = uid_counter;
        uid_counter++;
        level.grid.add_to_chunk (this, chunk_x, chunk_y);
        health = 100;
    }

    public virtual void update (float dt) {
        x += vx * dt;
        y += vy * dt;
        if (Maans.loop_counter%3 == 0)
            correct_chunk ();
    }

    private void correct_chunk () {
        int new_chunk_x = ChunkGrid.float_to_chunk (x);
        int new_chunk_y = ChunkGrid.float_to_chunk (y);

        if (new_chunk_x != chunk_x || new_chunk_y != chunk_y) {
            level.grid.remove_from_chunk (this, chunk_x, chunk_y);
            chunk_x = new_chunk_x;
            chunk_y = new_chunk_y;
            level.grid.add_to_chunk (this, chunk_x, chunk_y);
        }
    }

    protected void reduce_health (uint16 dmg) {
        if (dmg >= health) {
            health = 0;
        } else {
            health -= dmg;
        }
    }

    public virtual void damage (uint16 dmg) {
        reduce_health (dmg);
    }

    public float distance_to (float tx, float ty) {
        float distX = (x - tx);
        float distY = (y - ty);
        return (float) Math.sqrt (distX * distX + distY * distY);
    }

    public Point get_position () {
        return new Point ((int) x, (int) y);
    }

    public abstract void render (RenderBundle bundle);

    public abstract void render_light (RenderBundle bundle);

    public bool in_rect (FloatRect rect) {
        return rect.contains (x, y);
    }

}
