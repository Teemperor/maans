
public class RenderBundle {
    public SFML.Graphics.RenderTexture* texture;
    public SFML.Graphics.RenderWindow* window;
    public float dt;

    public RenderBundle(SFML.Graphics.RenderTexture* texture, SFML.Graphics.RenderWindow* window, float dt) {
        this.texture = texture;
        this.window = window;
        this.dt = dt;
    }
}
