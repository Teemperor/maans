using SFML.Window;
using SFML.Graphics;
using SFML.System;

public abstract class GameState {
    public abstract void update (float dt);
    public abstract void render (RenderBundle bundle);

    public abstract void next_state (ref GameState state);

    public virtual void pass_event (Event e) {}
}


public abstract class LoadingState : GameState {

    static Sprite* loading_cog;

    GameState return_state = null;

    GLib.Mutex load_mutex = GLib.Mutex ();

    bool running = false;

    private async void update_func () throws ThreadError {
        ThreadFunc<void*> run = () => {
            load_async ();
            return null;
        };
        new Thread<void*>("AsyncLoadingThread",run);
        yield;
    }

    public override void update (float dt) {
        loading_cog->rotate (dt * 50.0f);
        if(!running) {
            running = true;
            update_func.begin ();
        }
        load_step ();
    }

    public override void render (RenderBundle bundle) {
        var window_size = bundle.window->size;
        loading_cog->position = {window_size.x/2, window_size.y/2};
        bundle.window->draw_sprite (loading_cog, null);
    }

    public override void next_state (ref GameState state) {
        load_mutex.lock ();
        if(return_state != null)
            state = return_state;
        load_mutex.unlock ();
    }

    public virtual void load_async () {}

    public virtual void load_step () {}

    protected void finish_to_state (GameState state) {
        load_mutex.lock ();
        return_state = state;
        load_mutex.unlock ();

    }

    public static void init () {
        loading_cog = Loader.get_instance ().sprite("misc", "data/cog.png", true);
        var size = loading_cog->global_bounds;
        loading_cog->origin = {size.width/2, size.width/2};
    }
}

public class LevelLoadingState : LoadingState {

    private PlayState end_state;
    private Level level;

    public LevelLoadingState (PlayState state, Level level) {
        this.level = level;
        this.end_state = state;
    }

    public override void load_async () {
        level.post_init ();
        finish_to_state (end_state);
    }

}

public class PlayState : GameState {
    Game g;

    private bool init = false;

    public PlayState () {
        g = new Game ();
    }

    public override void update (float dt) {
        if (init)
            g.update (dt);
    }

    public override void render (RenderBundle bundle) {
        if (init)
            g.render (bundle);
    }

    public override void next_state (ref GameState state) {
        if (!init) {
            state = new LevelLoadingState (this, g.level);
            init = true;
        }
    }

    public override void pass_event (Event e) {
        g.pass_event (e);
    }
}
