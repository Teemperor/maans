using SFML.Window;
using SFML.System;

[CCode (cprefix = "sf", cheader_filename = "SFML/Graphics.h")]
namespace SFML.Graphics {
	/*
	 * Color
	 */

	[CCode (cname = "sfColor")]
	[SimpleType]
	/**
	 * Utility class for manpulating RGBA colors.
	 */
	public struct Color {
		public uint8 r;
		public uint8 g;
		public uint8 b;
		public uint8 a;

		[CCode (cname = "sfColor_fromRGB")]
		public static Color from_rgb (uint8 red, uint8 green, uint8 blue);

		[CCode (cname = "sfColor_fromRGBA")]
		public static Color from_rgba (uint8 red, uint8 green, uint8 blue, uint8 alpha);

		[CCode (cname = "sfColor_add")]
		public static Color add (Color c1, Color c2);

		[CCode (cname = "sfColor_modulate")]
		public static Color modulate (Color c1, Color c2);

		//predefined colors
		[CCode (cname = "sfBlack")]
		public static Color black;

		[CCode (cname = "sfWhite")]
		public static Color white;

		[CCode (cname = "sfRed")]
		public static Color red;

		[CCode (cname = "sfGreen")]
		public static Color green;

		[CCode (cname = "sfBlue")]
		public static Color blue;

		[CCode (cname = "sfYellow")]
		public static Color yellow;

		[CCode (cname = "sfMagenta")]
		public static Color magenta;

		[CCode (cname = "sfCyan")]
		public static Color cyan;

		[CCode (cname = "sfTransparent")]
		public static Color transparent;
	}


	/*
	 * ConvexShape
	 */

	[CCode (cname = "sfConvexShape", copy_function = "sfConvexShape_copy", free_function = "sfConvexShape_destroy")]
	[Compact]
	public class ConvexShape {
		[CCode (cname = "sfConvexShape_create")]
		public ConvexShape ();

		public Vector2f position {
			[CCode (cname = "sfConvexShape_getPosition")]
			get;
			[CCode (cname = "sfConvexShape_setPosition")]
			set;
		}

		public Vector2f scale {
			[CCode (cname = "sfConvexShape_getScale")]
			get;
			[CCode (cname = "sfConvexShape_setScale")]
			set;
		}

		public Vector2f origin {
			[CCode (cname = "sfConvexShape_getOrigin")]
			get;
			[CCode (cname = "sfConvexShape_setOrigin")]
			set;
		}

		public float rotation {
			[CCode (cname = "sfConvexShape_getRotation")]
			get;
			[CCode (cname = "sfConvexShape_setRotation")]
			set;
		}

		public Transform transform {
			[CCode (cname = "sfConvexShape_getTransform")]
			get;
		}

		public Transform inverse_transform {
			[CCode (cname = "sfConvexShape_getInverseTransform")]
			get;
		}

		public Color outline_color {
			[CCode (cname = "sfConvexShape_getOutlineColor")]
			get;
			[CCode (cname = "sfConvexShape_setOutlineColor")]
			set;
		}

		public Color fill_color {
			[CCode (cname = "sfConvexShape_getFillColor")]
			get;
			[CCode (cname = "sfConvexShape_setFillColor")]
			set;
		}

		public IntRect texture_rect {
			[CCode (cname = "sfConvexShape_getTextureRect")]
			get;
			[CCode (cname = "sfConvexShape_setTextureRect")]
			set;
		}

		public FloatRect local_bounds {
			[CCode (cname = "sfConvexShape_getLocalBounds")]
			get;
		}

		public FloatRect global_bounds {
			[CCode (cname = "sfConvexShape_getGlobalBounds")]
			get;
		}

		public float outline_thickness {
			[CCode (cname = "sfConvexShape_getOutlineThickness")]
			get;
			[CCode (cname = "sfConvexShape_setOutlineThickness")]
			set;
		}

		[CCode (cname = "sfConvexShape_move")]
		public void move (Vector2f offset);

		[CCode (cname = "sfConvexShape_rotate")]
		public void rotate (float angle);

		[CCode (cname = "sfConvexShape_scale")]
		public void do_scale (Vector2f factors);

		[CCode (cname = "sfConvexShape_setTexture")]
		public void set_texture (Texture texture, bool resetRect);

		[CCode (cname = "sfConvexShape_getTexture")]
		public Texture get_texture ();

		[CCode (cname = "sfConvexShape_getPointCount")]
		public uint get_point_count ();

		[CCode (cname = "sfConvexShape_getPoint")]
		public Vector2f get_point (uint index);

		[CCode (cname = "sfConvexShape_setPointCount")]
		public void set_point_count (uint count);

		[CCode (cname = "sfConvexShape_setPoint")]
		public void set_point (uint index, Vector2f point);
	}




	/*
	 * Blend mode
	 */

	public enum BlendMode {
		[CCode (cname = "sfBlendAlpha")]
		ALPHA,
		[CCode (cname = "sfBlendAdd")]
		ADD,
		[CCode (cname = "sfBlendMultiply")]
		MULTIPLY,
		[CCode (cname = "sfBlendNone")]
		NONE
	}


	/*
	 * Rect
	 */
	[SimpleType]
	public struct FloatRect {
		[CCode (cname = "left")]
		public float x;
		[CCode (cname = "top")]
		public float y;
		[CCode (cname = "width")]
		public float width;
		[CCode (cname = "height")]
		public float height;
		public bool contains (float x, float y) {
			return contains_internal (ref this, x, y);
		}
		[CCode (cname = "sfFloatRect_contains")]
		private static bool contains_internal (ref FloatRect r, float x, float y);
		public bool intersects (out FloatRect rect, out FloatRect intersection) {
			return intersects_internal (ref this, out rect, out intersection);
		}
		[CCode (cname = " sfFloatRect_intersects")]
		private static bool intersects_internal (ref FloatRect r, out FloatRect rect, out FloatRect intersection);
	}

	[SimpleType]
	public struct IntRect {
		[CCode (cname = "left")]
		public int x;
		[CCode (cname = "top")]
		public int y;
		[CCode (cname = "width")]
		public int width;
		[CCode (cname = "height")]
		public int height;

		public bool contains (int x, int y) {
			return contains_internal (ref this, x, y);
		}
		[CCode (cname = "sfIntRect_contains")]
		private static bool contains_internal (ref IntRect r, int x, int y);

		public bool intersects (out IntRect rect, out IntRect intersection) {
			return intersects_internal (ref this, out rect, out intersection);
		}
		[CCode (cname = " sfIntRect_intersects")]
		private static bool intersects_internal (ref IntRect r, out IntRect rect, out IntRect intersection);
	}




	/*
	 * Transform
	 */
	public struct Transform {
		[CCode (cname = "matrix")]
		public float matrix[9];

		[CCode (cname = "sfTransform_fromMatrix")]
		public static Transform from_matrix(float a00, float a01, float a02,
                                                      float a10, float a11, float a12,
                                                      float a20, float a21, float a22);

		[CCode (cname = "sfTransform_getMatrix")]
		public void getMatrix(float* matrix);

		[CCode (cname = "sfTransform_getInverse")]
		public Transform get_inverse ();

		[CCode (cname = "sfTransform_transformPoint")]
		public Vector2f get_transform_point (Vector2f point);

		[CCode (cname = "sfTransform_transformRect")]
		public FloatRect transformRect (FloatRect rectangle);

		[CCode (cname = "sfTransform_combine")]
		public void combine(Transform other);

		[CCode (cname = "sfTransform_translate")]
		public void translate(float x, float y);

		[CCode (cname = "sfTransform_rotate")]
		public void rotate(float angle);

		[CCode (cname = "sfTransform_rotateWithCenter")]
		public void rotate_with_center(float angle, float center_x, float center_y);

		[CCode (cname = "sfTransform_scale")]
		public void scale(float scaleX, float scaleY);

		[CCode (cname = "sfTransform_WithCenter")]
		public void scale_with_center(float scale_x, float scale_y, float center_x, float center_y);
	}




	/*
	 * Transformable
	 */

	[CCode (cname = "sfTransformable", copy_function = "sfTransformable_copy", free_function = "sfTransformable_destroy")]
	[Compact]
	public class Transformable {
		[CCode (cname = "sfTransformable_create")]
		public Transformable();

		public Vector2f position {
			[CCode (cname = "sfTransformable_getPosition")]
			get;
			[CCode (cname = "sfTransformable_setPosition")]
			set;
		}

		public Vector2f scale {
			[CCode (cname = "sfTransformable_getScale")]
			get;
			[CCode (cname = "sfTransformable_setScale")]
			set;
		}

		public Vector2f origin {
			[CCode (cname = "sfTransformable_getOrigin")]
			get;
			[CCode (cname = "sfTransformable_setOrigin")]
			set;
		}

		public float rotation {
			[CCode (cname = "sfTransformable_getRotation")]
			get;
			[CCode (cname = "sfTransformable_setRotation")]
			set;
		}

		public Transform transform {
			[CCode (cname = "sfTransformable_getTransform")]
			get;
		}

		public Transform inverse_transform {
			[CCode (cname = "sfTransformable_getInverseTransform")]
			get;
		}

		[CCode (cname = "sfTransformable_move")]
		public void move (Vector2f offset);

		[CCode (cname = "sfTransformable_rotate")]
		public void rotate (float angle);

		[CCode (cname = "sfTransformable_scale")]
		public void do_scale (Vector2f factors);
	}




	/*
	 * CircleShape
	 */

	[CCode (cname = "sfCircleShape", copy_function = "sfCircleShape_copy", free_function = "sfCircleShape_destroy")]
	[Compact]
	public class CircleShape {
		[CCode (cname = "sfCircleShape_create")]
		public CircleShape ();

		public Vector2f position {
			[CCode (cname = "sfCircleShape_getPosition")]
			get;
			[CCode (cname = "sfCircleShape_setPosition")]
			set;
		}

		public Vector2f scale {
			[CCode (cname = "sfCircleShape_getScale")]
			get;
			[CCode (cname = "sfCircleShape_setScale")]
			set;
		}

		public Vector2f origin {
			[CCode (cname = "sfCircleShape_getOrigin")]
			get;
			[CCode (cname = "sfCircleShape_setOrigin")]
			set;
		}

		public float rotation {
			[CCode (cname = "sfCircleShape_getRotation")]
			get;
			[CCode (cname = "sfCircleShape_setRotation")]
			set;
		}

		public Transform transform {
			[CCode (cname = "sfCircleShape_getTransform")]
			get;
		}

		public Transform inverse_transform {
			[CCode (cname = "sfCircleShape_getInverseTransform")]
			get;
		}

		public Color outline_color {
			[CCode (cname = "sfCircleShape_getOutlineColor")]
			get;
			[CCode (cname = "sfCircleShape_setOutlineColor")]
			set;
		}

		public Color fill_color {
			[CCode (cname = "sfCircleShape_getFillColor")]
			get;
			[CCode (cname = "sfCircleShape_setFillColor")]
			set;
		}

		public IntRect texture_rect {
			[CCode (cname = "sfCircleShape_getTextureRect")]
			get;
			[CCode (cname = "sfCircleShape_setTextureRect")]
			set;
		}

		public FloatRect local_bounds {
			[CCode (cname = "sfCircleShape_getLocalBounds")]
			get;
		}

		public FloatRect global_bounds {
			[CCode (cname = "sfCircleShape_getGlobalBounds")]
			get;
		}

		public float outline_thickness {
			[CCode (cname = "sfCircleShape_getOutlineThickness")]
			get;
			[CCode (cname = "sfCircleShape_setOutlineThickness")]
			set;
		}

		public float radius {
			[CCode (cname = "sfCircleShape_getRadius")]
			get;
			[CCode (cname = "sfCircleShape_setRadius")]
			set;
		}

		[CCode (cname = "sfCircleShape_setPointCount")]
		public void set_point_count (uint count);

		[CCode (cname = "sfCircleShape_getPointCount")]
		public uint get_point_count ();

		[CCode (cname = "sfCircleShape_getPoint")]
		public Vector2f get_point (uint index);

		[CCode (cname = "sfCircleShape_getTexture")]
		public Texture get_texture ();

		[CCode (cname = "sfCircleShape_setTexture")]
		public void set_texture (Texture texture, bool resetRect);

		[CCode (cname = "sfCircleShape_move")]
		public void move (Vector2f offset);

		[CCode (cname = "sfCircleShape_rotate")]
		public void rotate (float angle);

		[CCode (cname = "sfCircleShape_scale")]
		public void do_scale (Vector2f factors);
	}




	/*
	 * Font
	 */

	[CCode (cname = "sfFont", copy_function = "sfFont_copy", free_function = "sfFont_destroy")]
	[Compact]
	public class Font {
		[CCode (cname = "sfFont_createFromFile")]
		public Font (string filename);

		[CCode (cname = "sfFont_createFromMemory")]
		public Font.from_memory (void* data, size_t size_in_bytes);

		[CCode (cname = "sfFont_createFromStream")]
		public Font.from_stream (InputStream stream);

		[CCode (cname = "sfFont_getGlyph")]
		public Glyph get_glyph (uint32 code_point, uint character_size, bool bold);

		[CCode (cname = "sfFont_getKerning")]
		public int get_kerning (uint32 first, uint32 second, uint character_size);

		[CCode (cname = "sfFont_getLineSpacing")]
		public int get_line_spacing (uint character_size);

		[CCode (cname = "sfFont_getTexture")]
		public Texture get_texture (uint character_size);
	}




	/*
	 * Glyph
	 */

	[SimpleType]
	public struct Glyph {
		[CCode (cname = "advance")]
		public int advance;
		[CCode (cname = "bounds")]
		public IntRect bounds;
		[CCode (cname = "textureRect")]
		public IntRect texture_rect;
	}




	/*
	 * Image
	 */

	[CCode (cname = "sfImage", copy_function = "sfImage_copy", free_function = "sfImage_destroy")]
	[Compact]
	public class Image {
		[CCode (cname = "sfImage_create")]
		public Image (uint width, uint height);

		[CCode (cname = "sfImage_createFromColor")]
		public Image.from_color (uint width, uint height, Color color);

		[CCode (cname = "sfImage_createFromPixels")]
		public Image.from_pixels (uint width, uint height, uint8* data);

		[CCode (cname = "sfImage_createFromFile")]
		public Image.from_file (string filename);

		[CCode (cname = "sfImage_createFromMemory")]
		public Image.from_memory (void* data, size_t size);

		[CCode (cname = "sfImage_createFromStream")]
		public Image.from_stream (InputStream stream);

		[CCode (cname = "sfImage_saveToFile")]
		public bool save_to_file (string filename);

		public Vector2u size {
			[CCode (cname = "sfImage_getSize")]
			get;
		}

		[CCode (cname = "sfImage_createMaskFromColor")]
		public void create_mask_from_color (Color color_key, uint8 alpha);

		[CCode (cname = "sfImage_copyImage")]
		public void copy (Image source, uint dest_x, uint dest_y, IntRect source_rect, bool apply_alpha = true);

		[CCode (cname = "sfImage_setPixel")]
		public void set_pixel (uint x, uint y, Color color);

		[CCode (cname = "sfImage_getPixel")]
		public Color get_pixel (uint x, uint y);

		[CCode (cname = "sfImage_getPixelsPtr")]
		public uint8* get_pixels_pointer ();

		[CCode (cname = "sfImage_flipHorizontally")]
		public void flip_horizontally ();

		[CCode (cname = "sfImage_flipVertically")]
		public void flip_vertically ();
	}




	/*
	 * PrimitiveType
	 */

	public enum PrimitiveType {
		POINTS,
		LINES,
		LINES_STRIP,
		TRIANGLES,
		TRIANGLES_STRIP,
		TRIANGLES_FAN,
		QUADS
	}




	/*
	 * RectangleShape
	 */

	[CCode (cname = "sfRectangleShape", copy_function = "sfRectangleShape_copy", free_function = "sfRectangleShape_destroy")]
	[Compact]
	public class RectangleShape {
		[CCode (cname = "sfRectangleShape_create")]
		public RectangleShape ();

		public Vector2f position {
			[CCode (cname = "sfRectangleShape_getPosition")]
			get;
			[CCode (cname = "sfRectangleShape_setPosition")]
			set;
		}

		public Vector2f scale {
			[CCode (cname = "sfRectangleShape_getScale")]
			get;
			[CCode (cname = "sfRectangleShape_setScale")]
			set;
		}

		public Vector2f origin {
			[CCode (cname = "sfRectangleShape_getOrigin")]
			get;
			[CCode (cname = "sfRectangleShape_setOrigin")]
			set;
		}

		public float rotation {
			[CCode (cname = "sfRectangleShape_getRotation")]
			get;
			[CCode (cname = "sfRectangleShape_setRotation")]
			set;
		}

		public Transform transform {
			[CCode (cname = "sfRectangleShape_getTransform")]
			get;
		}

		public Transform inverse_transform {
			[CCode (cname = "sfRectangleShape_getInverseTransform")]
			get;
		}

		public Color outline_color {
			[CCode (cname = "sfRectangleShape_getOutlineColor")]
			get;
			[CCode (cname = "sfRectangleShape_setOutlineColor")]
			set;
		}

		public Color fill_color {
			[CCode (cname = "sfRectangleShape_getFillColor")]
			get;
			[CCode (cname = "sfRectangleShape_setFillColor")]
			set;
		}

		public IntRect texture_rect {
			[CCode (cname = "sfRectangleShape_getTextureRect")]
			get;
			[CCode (cname = "sfRectangleShape_setTextureRect")]
			set;
		}

		public FloatRect local_bounds {
			[CCode (cname = "sfRectangleShape_getLocalBounds")]
			get;
		}

		public FloatRect global_bounds {
			[CCode (cname = "sfRectangleShape_getGlobalBounds")]
			get;
		}

		public float outline_thickness {
			[CCode (cname = "sfRectangleShape_getOutlineThickness")]
			get;
			[CCode (cname = "sfRectangleShape_setOutlineThickness")]
			set;
		}

		public Vector2f size {
			[CCode (cname = "sfRectangleShape_getSize")]
			get;
			[CCode (cname = "sfRectangleShape_setSize")]
			set;
		}

		[CCode (cname = "sfRectangleShape_getTexture")]
		public Texture get_texture ();

		[CCode (cname = "sfRectangleShape_setTexture")]
		public void set_texture (Texture texture, bool resetRect);

		[CCode (cname = "sfRectangleShape_getPointCount")]
		public uint get_point_count ();

		[CCode (cname = "sfRectangleShape_getPoint")]
		public Vector2f get_point (uint index);

		[CCode (cname = "sfRectangleShape_move")]
		public void move (Vector2f offset);

		[CCode (cname = "sfRectangleShape_rotate")]
		public void rotate (float angle);

		[CCode (cname = "sfRectangleShape_scale")]
		public void do_scale (Vector2f factors);
	}




	/*
	 * RenderStates
	 */

	public struct RenderStates {
		[CCode (cname = "blendMode")]
		public BlendMode blendMode;

		[CCode (cname = "transform")]
		public Transform transform;

		[CCode (cname = "texture")]
		public Texture texture;

		[CCode (cname = "shader")]
		public Shader shader;
	}




	/*
	 * RenderWindow
	 */

	[CCode (cname = "sfRenderWindow", free_function = "sfRenderWindow_destroy")]
	[Compact]
	public class RenderWindow : Window.Window {
		[CCode (cname = "sfRenderWindow_create")]
		public RenderWindow (VideoMode mode, string title, uint style, out ContextSettings settings);

		[CCode (cname = "sfRenderWindow_createUnicode")]
		public RenderWindow.create_unicode (VideoMode mode, string title, uint style, out ContextSettings settings);

		[CCode (cname = "sfRenderWindow_close")]
		public void close ();

		[CCode (cname = "sfRenderWindow_isOpen")]
		public bool is_open ();

		public ContextSettings settings {
			[CCode (cname = "sfRenderWindow_getSettings")]
			get;
		}

		[CCode (cname = "sfRenderWindow_pollEvent")]
		public bool poll_event (out Event event_to_fill);

		[CCode (cname = "sfRenderWindow_waitEvent")]
		public bool wait_event (out Event event_to_fill);

		public Vector2i position {
			[CCode (cname = "sfRenderWindow_getPosition")]
			get;
			[CCode (cname = "sfWindow_setPosition")]
			set;
		}

		public Vector2u size {
			[CCode (cname = "sfRenderWindow_getSize")]
			get;
			[CCode (cname = "sfRenderWindow_setSize")]
			set;
		}

		/**
		 * Title of the window.
		 */
		public string title {
			[CCode (cname = "sfRenderWindow_setTitle")]
			set;
		}

		public string unicode_title {
			[CCode (cname = "sfRenderWindow_setUnicodeTitle")]
			set;
		}

		[CCode (cname = "sfRenderWindow_setIcon")]
		public void set_icon (uint width, uint height, uint8* pixels);

		public bool visible {
			[CCode (cname = "sfRenderWindow_setVisible")]
			set;
		}

		public bool key_repeat {
			[CCode (cname = "sfRenderWindow_setKeyRepeatEnabled")]
			set;
		}

		public bool mouse_cursor_visible {
			[CCode (cname = "sfRenderWindow_setMouseCursorVisible")]
			set;
		}

		public bool vertical_sync {
			[CCode (cname = "sfRenderWindow_setVerticalSyncEnabled")]
			set;
		}

		public bool active {
			[CCode (cname = "sfRenderWindow_setActive")]
			set;
		}

		[CCode (cname = "sfRenderWindow_display")]
		public void display ();

		public uint framerate_limit {
			[CCode (cname = "sfRenderWindow_setFramerateLimit")]
			set;
		}

		public float joystick_threshold {
			[CCode (cname = "sfRenderWindow_setJoystickThreshold")]
			set;
		}

		[CCode (cname = "sfRenderWindow_clear")]
		public void clear (Color color);

		public unowned View view {
			[CCode (cname = "sfRenderWindow_getView")]
			get;
			[CCode (cname = "sfRenderWindow_setView")]
			set;
		}

		public unowned View default_view {
			[CCode (cname = "sfRenderWindow_getDefaultView")]
			get;
		}

		public IntRect viewport {
			[CCode (cname = "sfRenderWindow_getViewport")]
			get;
		}

		[CCode (cname = "sfRenderWindow_convertCoords")]
		protected Vector2f convert_coords (Vector2i point, out View? target_view);

		[CCode (cname = "sfRenderWindow_mapPixelToCoords")]
		public Vector2f map_pixel_to_coords (Vector2i point, View view);

		[CCode (cname = "sfRenderWindow_mapCoordsToPixel")]
		public Vector2i map_coords_to_pixel (Vector2f point, View view);

		[CCode (cname = "sfRenderWindow_drawSprite")]
		public void draw_sprite (Sprite sprite, RenderStates? states = null);

		[CCode (cname = "sfRenderWindow_drawText")]
		public void draw_text (Text text, RenderStates? states = null);

		[CCode (cname = "sfRenderWindow_drawShape")]
		public void draw_shape (Shape shape, RenderStates? states = null);

		[CCode (cname = "sfRenderWindow_drawCircleShape")]
		public void draw_circle_shape (CircleShape shape, RenderStates? states = null);

		[CCode (cname = "sfRenderWindow_drawConvexShape")]
		public void draw_convex_shape (ConvexShape shape, RenderStates? states = null);

		[CCode (cname = "sfRenderWindow_drawRectangleShape")]
		public void draw_rectangle_shape (RectangleShape shape, RenderStates? states = null);

		[CCode (cname = "sfRenderWindow_drawVertexArray")]
		public void draw_vertex_shape (VertexArray vertices, RenderStates? states = null);

		[CCode (cname = "sfRenderWindow_drawPrimitives")]
		public void draw_primitives (VertexArray vertices, uint vertex_count,
							PrimitiveType type, RenderStates? states = null);

		[CCode (cname = "sfRenderWindow_pushGLStates")]
		public void push_glstates ();

		[CCode (cname = "sfRenderWindow_popGLStates")]
		public void pop_glstates ();

		[CCode (cname = "sfRenderWindow_resetGLStates")]
		public void reset_glstates ();

		[CCode (cname = "capture")]
		public Image capture ();

		[CCode (cname = "sfMouse_getPositionRenderWindow")]
		public Vector2i get_position_render_window ();

		[CCode (cname = "sfMouse_setPositionRenderWindow")]
		public static void set_position_render_window (Vector2i position, RenderWindow relativeTo);
	}


	/*
	 * RenderTexture
	 */

	[CCode (cname = "sfRenderTexture", free_function = "sfRenderTexture_destroy")]
	[Compact]
	public class RenderTexture {
		[CCode (cname = "sfRenderTexture_create")]
		public RenderTexture (uint width, uint height, bool depth);

		[CCode (cname = "sfRenderTexture_close")]
		public void close ();

		[CCode (cname = "sfRenderTexture_getSize")]
		public Vector2u get_size ();

		public bool active {
			[CCode (cname = "sfRenderTexture_setActive")]
			set;
		}

		[CCode (cname = "sfRenderTexture_display")]
		public void display ();

		public bool smooth {
			[CCode (cname = "sfRenderTexture_isSmooth")]
			get;
			[CCode (cname = "sfRenderTexture_setSmooth")]
			set;
		}

		public uint framerate_limit {
			[CCode (cname = "sfRenderTexture_setFramerateLimit")]
			set;
		}

		public float joystick_threshold {
			[CCode (cname = "sfRenderTexture_setJoystickThreshold")]
			set;
		}

		[CCode (cname = "sfRenderTexture_clear")]
		public void clear (Color color);

		public unowned View view {
			get;
			set;
		}

		[CCode (cname = "sfRenderTexture_setView")]
		public void set_view (View view);

		[CCode (cname = "sfRenderTexture_getView")]
		public unowned View get_view ();

		[CCode (cname = "sfRenderTexture_getDefaultView")]
		public unowned View get_default_view ();

		[CCode (cname = "sfRenderTexture_getViewport")]
		public IntRect get_viewport (View view);

		[CCode (cname = "sfRenderTexture_getTexture")]
		public Texture get_texture ();

		[CCode (cname = "sfRenderTexture_mapPixelToCoords")]
		public Vector2f mappixel_tocoords (Vector2i point, out View? target_view);

		[CCode (cname = "sfRenderTexture_drawSprite")]
		public void draw_sprite (Sprite sprite, RenderStates? states = null);

		[CCode (cname = "sfRenderTexture_drawText")]
		public void draw_text (Text text, RenderStates? states = null);

		[CCode (cname = "sfRenderTexture_drawShape")]
		public void draw_shape (Shape shape, RenderStates? states = null);

		[CCode (cname = "sfRenderTexture_drawCircleShape")]
		public void draw_circle_shape (CircleShape shape, RenderStates? states = null);

		[CCode (cname = "sfRenderTexture_drawConvexShape")]
		public void draw_convex_shape (ConvexShape shape, RenderStates? states = null);

		[CCode (cname = "sfRenderTexture_drawRectangleShape")]
		public void draw_rectangle_shape (RectangleShape shape, RenderStates? states = null);

		[CCode (cname = "sfRenderTexture_drawVertexArray")]
		public void draw_vertex_shape (VertexArray vertices, RenderStates? states = null);

		[CCode (cname = "sfRenderTexture_drawPrimitives")]
		public void draw_primitives (VertexArray vertices, uint vertex_count,
							PrimitiveType type, RenderStates? states = null);

		[CCode (cname = "sfRenderTexture_pushGLStates")]
		public void push_glstates ();

		[CCode (cname = "sfRenderTexture_popGLStates")]
		public void pop_glstates ();

		[CCode (cname = "sfRenderTexture_resetGLStates")]
		public void reset_glstates ();
	}


	/*
	 * Shader
	 */

	[CCode (cname = "sfShader", copy_function = "sfShader_copy", free_function = "sfShader_destroy")]
	[Compact]
	public class Shader {
		[CCode (cname = "sfShader_create")]
		public Shader (string vertexShaderFilename, string fragmentShaderFilename);

		[CCode (cname = "sfShader_createFromMemory")]
		public  Shader.from_memory (string? vertexShader, string? fragmentShader);

		[CCode (cname = "sfShader_createFromStream")]
		public  Shader.from_stream (InputStream vertexShaderStream, InputStream fragmentShaderStream);

		[CCode (cname = "sfShader_setFloatParameter")]
		public void set_float_param (string name, float x);

		[CCode (cname = "sfShader_setFloat2Parameter")]
		public void set_float_2param (string name, float x, float y);

		[CCode (cname = "sfShader_setFloat3Parameter")]
		public void set_float_3param (string name, float x, float y, float z);

		[CCode (cname = "sfShader_setFloat4Parameter")]
		public void set_float_4param (string name, float x, float y, float z, float w);

		[CCode (cname = "sfShader_setVector2Parameter")]
		public void set_vector_2param (string name, Vector2f vector);

		[CCode (cname = "sfShader_setVector3Parameter")]
		public void set_vector_3param (string name, Vector3f vector);

		[CCode (cname = "sfShader_setColorParameter")]
		public void set_color_param (string name, Color color);

		[CCode (cname = "sfShader_setTransformParameter")]
		public void set_transform_param (string name, Transform transform);

		[CCode (cname = "sfShader_setTextureParameter")]
		public void set_texture_param (string name, Texture* texture);

		[CCode (cname = "sfShader_setCurrentTextureParameter")]
		public void set_current_texture_param (string name);

		[CCode (cname = "sfShader_isAvailable")]
		public bool is_available ();

		[CCode (cname = "sfShader_bind")]
		public static void bind (Shader? s);
	}


	/*
	 * Shape
	 */

	[CCode (cname = "sfShapeGetPointCountCallback")]
	public delegate uint GetPointCountCallback (void* arg);

	[CCode (cname = "sfShapeGetPointCallback")]
	public delegate Vector2f GetPointCallback (uint arg0, void* arg1);

	[CCode (cname = "sfShape", copy_function = "sfShape_copy", free_function = "sfShape_destroy")]
	[Compact]
	public class Shape {
		[CCode (cname = "sfShape_create")]
		public Shape (GetPointCountCallback getPointCount,
					GetPointCallback getPoint,
					void* userData);

		public Vector2f position {
			[CCode (cname = "sfShape_getPosition")]
			get;
			[CCode (cname = "sfShape_setPosition")]
			set;
		}

		public Vector2f scale {
			[CCode (cname = "sfShape_getScale")]
			get;
			[CCode (cname = "sfShape_setScale")]
			set;
		}

		public Vector2f origin {
			[CCode (cname = "sfShape_getOrigin")]
			get;
			[CCode (cname = "sfShape_setOrigin")]
			set;
		}

		public float rotation {
			[CCode (cname = "sfShape_getRotation")]
			get;
			[CCode (cname = "sfShape_setRotation")]
			set;
		}

		public Transform transform {
			[CCode (cname = "sfShape_getTransform")]
			get;
		}

		public Transform inverse_transform {
			[CCode (cname = "sfShape_getInverseTransform")]
			get;
		}

		public Color outline_color {
			[CCode (cname = "sfShape_getOutlineColor")]
			get;
			[CCode (cname = "sfShape_setOutlineColor")]
			set;
		}

		public Color fill_color {
			[CCode (cname = "sfShape_getFillColor")]
			get;
			[CCode (cname = "sfShape_setFillColor")]
			set;
		}

		public IntRect texture_rect {
			[CCode (cname = "sfShape_getTextureRect")]
			get;
			[CCode (cname = "sfShape_setTextureRect")]
			set;
		}

		public FloatRect local_bounds {
			[CCode (cname = "sfShape_getLocalBounds")]
			get;
		}

		public FloatRect global_bounds {
			[CCode (cname = "sfShape_getGlobalBounds")]
			get;
		}

		public float outline_thickness {
			[CCode (cname = "sfShape_getOutlineThickness")]
			get;
			[CCode (cname = "sfShape_setOutlineThickness")]
			set;
		}

		[CCode (cname = "sfShape_update")]
		public void update ();

		[CCode (cname = "sfShape_move")]
		public void move (Vector2f offset);

		[CCode (cname = "sfShape_scale")]
		public void do_scale (Vector2f factors);

		[CCode (cname = "sfShape_rotate")]
		public void rotate (float angle);

		[CCode (cname = "sfShape_setTexture")]
		public void set_texture (Texture texture, bool reset_rect = false);

		[CCode (cname = "sfShape_getTexture")]
		public Texture get_texture ();

		[CCode (cname = "sfShape_getPointsCount")]
		public uint get_points_count ();

		[CCode (cname = "sfShape_getPoint")]
		public Vector2f get_point (uint index);
	}




	/*
	 * Sprite
	 */
	[CCode (cname = "sfSprite", copy_function = "sfSprite_copy", free_function = "sfSprite_destroy")]
    [Compact]
	public class Sprite {
		[CCode (cname = "sfSprite_create")]
		public Sprite ();

		public Vector2f position {
			[CCode (cname = "sfSprite_getPosition")]
			get;
			[CCode (cname = "sfSprite_setPosition")]
			set;
		}

		public Vector2f scale {
			[CCode (cname = "sfSprite_getScale")]
			get;
			[CCode (cname = "sfSprite_setScale")]
			set;
		}

		public FloatRect global_bounds {
			[CCode (cname = "sfSprite_getGlobalBounds")]
			get;
		}

		public FloatRect local_bounds {
			[CCode (cname = "sfSprite_getLocalBounds")]
			get;
		}

		public float rotation {
			[CCode (cname = "sfSprite_getRotation")]
			get;
			[CCode (cname = "sfSprite_setRotation")]
			set;
		}

		public Vector2f origin {
			[CCode (cname = "sfSprite_getOrigin")]
			get;
			[CCode (cname = "sfSprite_setOrigin")]
			set;
		}

		public Color color {
			[CCode (cname = "sfSprite_getColor")]
			get;
			[CCode (cname = "sfSprite_setColor")]
			set;
		}

		public IntRect texture_rect {
			[CCode (cname = "sfSprite_getTextureRect")]
			get;
			[CCode (cname = "sfSprite_setTextureRect")]
			set;
		}

		public Transform transform {
			[CCode (cname = "sfSprite_getTransform")]
			get;
		}

		public Transform inverse_transform {
			[CCode (cname = "sfSprite_getInverseTransform")]
			get;
		}

		public unowned Texture texture {
			[CCode (cname = "sfSprite_getTexture")]
			get;
			set {
				set_texture_full (value, false);
			}
		}

		[CCode (cname = "sfSprite_move")]
		public void move (Vector2f offset);

		[CCode (cname = "sfSprite_scale")]
		public void do_scale (Vector2f factors);

		[CCode (cname = "sfSprite_rotate")]
		public void rotate (float angle);

		[CCode (cname = "sfSprite_setTexture")]
		public void set_texture_full (Texture texture, bool reset_rect);

	}




	/*
	 * Text
	 */

	public enum TextStyle {
		[CCode (cname = "sfTextRegular")]
		REGULAR = 0,
		[CCode (cname = "sfTextBold")]
		BOLD = 1 << 0,
		[CCode (cname = "sfTextItalic")]
		ITALIC = 1 << 1,
		[CCode (cname = "sfTextUnderlined")]
		UNDERLINED = 1 << 2
	}

	[CCode (cname = "sfText", copy_function = "sfText_copy", free_function = "sfText_destroy")]
	[Compact]
	public class Text {
		[CCode (cname = "sfText_create")]
		public Text ();

		public Vector2f position {
			[CCode (cname = "sfText_getPosition")]
			get;
			[CCode (cname = "sfShape_setPosition")]
			set;
		}

		public Vector2f scale {
			[CCode (cname = "sfText_getScale")]
			get;
			[CCode (cname = "sfShape_setScale")]
			set;
		}

		public Vector2f origin {
			[CCode (cname = "sfText_getOrigin")]
			get;
			[CCode (cname = "sfText_setOrigin")]
			set;
		}

		public float rotation {
			[CCode (cname = "sfText_getRotation")]
			get;
			[CCode (cname = "sfText_setRotation")]
			set;
		}

		public Transform transform {
			[CCode (cname = "sfText_getTransform")]
			get;
		}

		public Transform inverse_transform {
			[CCode (cname = "sfText_getInverseTransform")]
			get;
		}

		public string text {
			[CCode (cname = "sfText_getString")]
			get;
			[CCode (cname = "sfText_setString")]
			set;
		}

		public uint32* unicode_text {
			[CCode (cname = "sfText_getUnicodeString")]
			get;
			[CCode (cname = "sfText_setUnicodeString")]
			set;
		}

		public Font font {
			[CCode (cname = "sfText_getFont")]
			get;
			[CCode (cname = "sfText_setFont")]
			set;
		}

		public Color color {
			[CCode (cname = "sfText_getColor")]
			get;
			[CCode (cname = "sfText_setColor")]
			set;
		}

		public uint character_size {
			[CCode (cname = "sfText_getCharacterSize")]
			get;
			[CCode (cname = "sfText_setCharacterSize")]
			set;
		}

		public TextStyle style {
			[CCode (cname = "sfText_getStyle")]
			get;
			[CCode (cname = "sfText_setStyle")]
			set;
		}

		public FloatRect local_bounds {
			[CCode (cname = "sfText_getLocalBounds")]
			get;
		}

		public FloatRect global_bounds {
			[CCode (cname = "sfText_getGlobalBounds")]
			get;
		}

		[CCode (cname = "sfText_move")]
		public void move (Vector2f offset);

		[CCode (cname = "sfText_scale")]
		public void do_scale (Vector2f factors);

		[CCode (cname = "sfText_rotate")]
		public void rotate (float angle);

		[CCode (cname = "sfText_findCharacterPos")]
		public Vector2f find_char_pos (ulong index);
	}


	/*
	 * Texture
	 */

	[CCode (cname = "sfTexture", copy_function = "sfTexture_copy", free_function = "sfTexture_destroy")]
	[Compact]
	public class Texture {
		[CCode (cname = "sfTexture_create")]
		public Texture (uint width, uint height);

		[CCode (cname = "sfTexture_createFromImage")]
		public Texture.from_image (Image image, ref IntRect area);

		[CCode (cname = "sfTexture_createFromFile")]
		public Texture.from_file (string filename, ref IntRect area);

		[CCode (cname = "sfTexture_createFromMemory")]
		public Texture.from_memory (void* data, size_t size);

		[CCode (cname = "sfTexture_createFromStream")]
		public Texture.from_stream (InputStream stream);

		[CCode (cname = "sfTexture_copyToImage")]
		public Image copy_to_image ();

		public bool smooth {
			[CCode (cname = "sfTexture_isSmooth")]
			get;
			[CCode (cname = "sfTexture_setSmooth")]
			set;
		}

		public Vector2u size {
			[CCode (cname = "sfTexture_getSize")]
			get;
		}

		public bool repeated {
			[CCode (cname = "sfTexture_isRepeated")]
			get;
			[CCode (cname = "sfTexture_setRepeated")]
			set;
		}

		[CCode (cname = "sfTexture_getMaximumSize")]
		public static uint get_max_size ();

		[CCode (cname = "sfTexture_updateFromPixels")]
		public void update_from_pixels (uint8* pixels, Image image, uint x, uint y);

		[CCode (cname = "sfTexture_updateFromImage")]
		public void update_from_image (Image image, uint x = 0, uint y = 0);

		[CCode (cname = "sfTexture_updateFromWindow")]
		public void update_from_window (uint8* pixels, Window.Window window, uint x, uint y);

		[CCode (cname = "sfTexture_updateFromRenderWindow")]
		public void update_from_render_window (uint8* pixels, RenderWindow window, uint x, uint y);

		[CCode (cname = "sfTexture_bind")]
		public void bind ();
	}




	/*
	 * Vertex
	 */

	[SimpleType]
	public struct Vertex{
		[CCode (cname = "position")]
		Vector2f position;

		[CCode (cname = "color")]
		Color color;

		[CCode (cname = "texCoords")]
		Vector2f tex_coords;
	}



	/*
	 * VertexArray
	 */

	[CCode (cname = "sfVertexArray", copy_function = "sfVertexArray_copy", free_function = "sfVertexArray_destroy")]
	[Compact]
	public class VertexArray {
		[CCode (cname = "sfVertexArray_create")]
		public VertexArray ();

		public uint vertex_count {
			[CCode (cname = "sfVertexArray_getVertexCount")]
			get;
		}

		[CCode (cname = "sfVertexArray_getVertex")]
		public Vertex get_vertex (uint index);

		[CCode (cname = "sfVertexArray_clear")]
		public void clear ();

		[CCode (cname = "sfVertexArray_resize")]
		public void resize (uint vertex_count);

		[CCode (cname = "sfVertexArray_append")]
		public void append (Vertex vertex);

		public PrimitiveType primitive_type {
			[CCode (cname = "sfVertexArray_getPrimitiveType")]
			get;
			[CCode (cname = "sfVertexArray_setPrimitiveType")]
			set;
		}

		public FloatRect bounds {
			[CCode (cname = "sfVertexArray_getBounds")]
			get;
		}
	}




	/*
	 * View
	 */

	[CCode (cname = "sfView", copy_function = "sfView_copy", free_function = "sfView_destroy")]
	[Compact]
	public class View {
		[CCode (cname = "sfView_create")]
		public View ();

		[CCode (cname = "sfView_createFromRect")]
		public View.from_rect (FloatRect rectangle);

		public Vector2f size {
			[CCode (cname = "sfView_getSize")]
			get;
			[CCode (cname = "sfView_setSize")]
			set;
		}

		public Vector2f center {
			[CCode (cname = "sfView_getCenter")]
			get;
			[CCode (cname = "sfView_setCenter")]
			set;
		}

		public float rotation {
			[CCode (cname = "sfView_getRotation")]
			get;
			[CCode (cname = "sfView_setRotation")]
			set;
		}

		public FloatRect viewport {
			[CCode (cname = "sfView_getViewport")]
			get;
			[CCode (cname = "sfView_setViewport")]
			set;
		}

		[CCode (cname = "sfView_reset")]
		public void reset (FloatRect rectangle);

		[CCode (cname = "sfView_move")]
		public void move (Vector2f offset);

		[CCode (cname = "sfView_rotate")]
		public void rotate (float angle);

		[CCode (cname = "sfView_zoom")]
		public void zoom (float factor);
	}
}
